/*=====================================================================
 Copyright (C) 2012-2013 Alain BENEDETTI alainb06@free.fr

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published byr
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>
=====================================================================*/

/*=====================================================================================\
| Function:                                                                            |
|==========                                                                            |
|  This utility implements a lock-free concurrent LIFO queue and a 'Message' queue.    |
|                                                                                      |
|  A LIFO queue is well suited for "pools" of resource you use (pop) and then release  |
|  to the "pool" once done with the resource.                                          |
|  It is meant to be as fast as possible on concurrent/multi-threaded situation, and   |
|  works for multiple users (N producers -push-, N consumers -pop-).                   |
|                                                                                      |
|  Please note that this utility is developed for Linux. Portability for other systems |
|  is not my concern.                                                                  |
|                                                                                      |
|  Four lock models are implemented:                                                   |
|  - none: meaning that you shouldn't use that in a multithreaded situation!           |
|  - standard locks with mutexes                                                       |
|  - simple CAS                                                                        |
|  - DCAS (on Intel processors, in fact the intel flavor which is double-pointer CAS)  |
|                                                                                      |
|  You can select the 'fastest' (see note) mode (DCAS) and the utility will fallback   |
|  to slower modes (CAS or even Mutex) if the fastest modes are not available.         |
|                                                                                      |
|  Note: the relative speed of the CAS/DCAS algorithm depends on the processor. It can |
|        be measured with the test program (lifo_multi) running it mono-threaded. DCAS |
|        mode is usually faster, as the algorithm is more straightforward much less    |
|        instructions are needed. But speed boils down to how many contention you get. |
|        On some situations you might bet more contention with DCAS thus making it     |
|        slower than CAS.                                                              |
|                                                                                      |
|  The 'Message' queue differs from the LIFO queue only for the 'pop' part. When a     |
|  consumer 'pops', it takes ALL the nodes that were queued. This pattern is useful    |
|  if you have a 'worker' thread and send 'work message' to this thread. Then the      |
|  worker receives a 'chain' of nodes and must handle them all.                        |
|  This difference (pop ALL) make a great simplification on the algorithms. DCAS       |
|  becomes unnecessary (fallbacks to CAS), and the CAS pop is wait-free!               |
|                                                                                      |
|  There can also be several consumers poping in the 'Message' patterns. It works      |
|  without modification of the present algorithm, although I fail to think of a useful |
|  use case of multiple consumers (only one of them will get ALL, and the other ones   |
|  will get nothing!).                                                                 |
|  There could also be a mixture of 'unitary consumers' (LIFO) and 'workers consumers' |
|  (MSG), this would require some modifications, especially for CAS algorithm, and MSG |
|  consumers won't be wait free anymore. Provision is made on some structures and on   |
|  some functions (init) to handle such a case, but it is not fully developped as I do |
|  also fail to see the usefulness of such a mixture! If you have a use case for that  |
|  feel free to submit it and I'll gladly do the appropriate modifications!            |
|                                                                                      |
|  This utility is not a program per se, but meant to be used by language C programs.  |
|                                                                                      |
|--------------------------------------------------------------------------------------|
| Tested: Ubuntu Precise (12.04 x64 and i386)                                          |
|========                                                                              |
|                                                                                      |
|--------------------------------------------------------------------------------------|
| Version: 0.2.0                                                                       |
|=========                                                                             |
|                                                                                      |
|--------------------------------------------------------------------------------------|
| Date: 2013-07-20                                                                     |
|======                                                                                |
|                                                                                      |
|--------------------------------------------------------------------------------------|
| Author: Alain BENEDETTI aka Zakhar                                                   |
|========                                                                              |
|                                                                                      |
|--------------------------------------------------------------------------------------|
| History:                                                                             |
|=========                                                                             |
| 0.2.0                                                                                |
|  * Added msg_reverse to prepare for C RCU-Garbage collector.                         |
|  * minor improvements                                                                |
|  * tested on my Synology: processor Marvell Kirkwood mv6282 (ARMv5te)                |
|--------------------------------------------------------------------------------------|
| Compiling instructions:                                                              |
|========================                                                              |
|                                                                                      |
|  You should use the provided Makefile. Default compile command in the Makefile is:   |
gcc -Wall -Wextra -std=gnu1x -D_GNU_SOURCE -O3 -mcx16 \
    -DLEVEL1_DCACHE_LINESIZE=64 \
    -DCHECK=1 \
    -DSTATS=0 \
    -DCAS_ENABLED=1 \
    -DTHRESHOLD=-1 \
    -DDEF_MODE=0 \
    -o lifo.o -c lifo.c                            
|                                                                                      |
|  You usually want to link with -pthreads, as it is meant to be multi-threaded.       |
|                                                                                      |
|  OPTIONS  : 6 options are available from the command line (see Symbols set on the    |
|             default command line above) or with the Makefile with the same names.    |
|    LEVEL1_DCACHE_LINESIZE                                                            |
|             This defaults to 64, which is the right value for Intel/AMD processor    |
|             targets. You can change it to the appropriate value for you processor    |
|             target, provided it is a power of 2 (greater or equal than 2)            |
|    CHECK                                                                             |
|             Defaults to 0, means that we want to check pointers passed to functions  |
|             Pointers are checked for NULL and for alignment when relevant. This is   |
|             safe. If you are sure you don't need it, feel free to set CHECK to 0     |
|             and you will save a few CPU cycles on each function call... with a risk  |
|             of coredump if pointers are not correct.                                 |
|    STATS                                                                             |
|             Defaults to 0. If set to 1, this enables the statistics.                 |
|             The number of statistics counter depends on the lock mode.               |
|             This option is both at compile time and at run time. If enabled at       |
|             compile time, statistics can be turned off or on at run time. When       |
|             disabled at compile time, no statistic code is generated (this is the    |
|             fastest option if you don't need stats) thus stats can't be turned on    |
|             at runtime.                                                              |
|    CAS_ENABLED                                                                       |
|             By default, it is enabled (1), unless gcc detects that your target has   |
|             no CAS. You can specify a value of 0 to disable it. This will leave you  |
|             with only the classic mutex lock implementation.                         |
|             Note that when CAS is disabled, DCAS is disabled too, be it by gcc auto- |
|             matic detection, on through this parameter.                              |
|    THRESHOLD                                                                         |
|             This sets the backoff threshold for CAS and DCAS mode. -1 (default) means|
|             no backoff. If set to -1 at compile time, no Backoff code is generated,  |
|             thus backoff cannot be activated later at run time. It the value is      |
|             not -1 it can be changed at run time to 0 (no backoff) or any value.     |
|             Setting this to non-zero will prevents live-lock but introduces mutex    |
|             lock in the CAS and DCAS algorithms.                                     |
|    DEF_MODE                                                                          |
|             This is the default scheme for locking. When none is given, we go on the |
|             safe and slow side and use locks. However, you can change this to CAS or |
|             DCAS... but bear in mind, DCAS is only tested at runtime. So if you      |
|             chose to make DCAS the default, queues with "default mode" might go to   |
|             CAS or LOCK, and in that case, the variable lifo_default_mode would have |
|             an "impossible" value until changed by lifo_init. If you specify CAS here|
|             and the target don't have CAS, this is detected by gcc and DEF_MODE will |
|             fallback to LOCK.                                                        |
|                                                                                      |
|  NOTE : the -mcx16 allows gcc to generate compare_and_swap on 16bytes (eg 128bits)   |
|         for 64bits targets, when such option is available.                           |
|  NOTE2: the compilation will generate warnings to inform about memory model, wether  |
|         we are compiling for Intel target, and the status of the 6 parameters above. |
|                                                                                      |
|--------------------------------------------------------------------------------------|
| General Notes:                                                                       |
|===============                                                                       |
|  First, DO read the excellent 1024cores.net website                                  |
|  http://www.1024cores.net/home/lock-free-algorithms/first-things-first               |
|  So, if your concern is speed, try to find another algorithm that works only with    |
|  thread-local variables, because obviously, we need to write on that shared head     |
|  of LIFO queue, and that collapses performance in a multi-core environment, even if  |
|  doing DCAS or CAS is generally better than the basic LOCK (mutex) algorithm.        |
|  For instance, if you are doing a LIFO queue to save malloc/free... think twice!     |
|  malloc/free (on Linux) is vastly thread local (caching allocs). It means that it    |
|  scales wonderfully the more threads you have to do N malloc/free, the quicker it    |
|  will be, contrary to the LIFO queue that slows down every thread you add.           |
|  To be accurate about that, malloc/free is indeed slower when you have only 1 thread |
|  but generally catches up as soon as you have 2 threads with some contention.        |
|                                                                                      |
| Algorithms summary:                                                                  |
|====================                                                                  |
|  We have 4 different algorithms.                                                     |
| -1) No lock at all!                                                                  |
|     Obviously it is NOT thread safe, and should be used only when the caller knows   |
|     there can be NO contention (either because it is run on a single thread, or      |
|     because the caller has another lock mechanism).                                  |
|     This algorithm is provided for convenience only, so that the progamer using      |
|     this library won't have to develop another LIFO algorithm when he needs single-  |
|     threaded LIFO. It stil does statistics and pointer checks if they are set.       |
| -2) Lock with mutexes (the classical approach)                                       |
|     This algorithm does ALWAYS work, and is ALWAYS available on any target processor |
|     where gcc exists. It is also, unfortunately, the slowest one and also has the    |
|     drawbacks of any algorithms with mutexes: if a thread stalls when a resource is  |
|     locked, your entire program might stall!                                         |
|     On the bright side, as we clearly lock out contender threads, it is the only     |
|     algorithm (obviously with the "no lock" algorithm too!) that allows reclamation  |
|     of the elements of the queue. The other ones need another mechanism to reclaim   |
|     the elements. But as you would generally use a LIFO queue for "pool-like"        |
|     mechanisms, it is expected that you do never need reclaiming memory from the     |
|     elements in the queue, or do it at program termination, after thread             |
|     cancellation, where it is perfectely safe to do so. So the non reclamation of    |
|     elements in other algorithms should be a minor inconvenience.                    |
| -3) CAS (simple CAS)                                                                 |
|     For "push", it is easy, we just CAS elements in the queue.                       |
|     For "pop", it is trickier, if we simply CAS, we can get "ABA-ed", and lose some  |
|     nodes. So we do it on 2 stages: first we CAS the head with a flag, then we do    |
|     the real operation. Whenever other contended threads start push/pop when the     |
|     head is marked, they will help complete the second stage of the process. The     |
|     problem is then that helpers can then get ABA-ed. To avoid that, the critical    |
|     section of the help process is protected by an access counter. On the push algo- |
|     rithm, we detect that, and if some thread is on it's critical section, we don't  |
|     push back the node but just mark it "pushable". It will then be pushed back by a |
|     helper, when no other helper is on it's critical section. This ensure we are not |
|     ABA-ed because the same node can't be pushed again till someone is in critical   |
|     section. Note that this algorithm has 2 side effects:                            |
|     a) Any thread can suffer from starvation on high contention. As demonstrated by  |
|     the lifo_multi program, if you run it on a core i7 proc, you might easily go up  |
|     to more than 100 retries. If that is not acceptable, you can solve it with the   |
|     backoff option (but then you get the same drawbacks as mutex locks).             |
|     b) Due to the 'helper' mechanism, if a thread stalls, it might 'steal' some      |
|     nodes from the queue. This could happen if a push was delayed and delegated to   |
|     helpers, and the helper that gets the token to push back stall. The rest of      |
|     the threads will continue to work with the remaining nodes and new nodes.        |
|     UNLIKELY SITUATION: ...always happen, or so they say in concurrent programming!  |
|       So, a very unlikely situation, is some threads "stealing" every node as soon   |
|       as it is pushed on the queue. This can theoretically happen, or more likely    |
|       would need to be especially crafted to happen! Anyway, contrary to starvation  |
|       it has not been observed during our tests. The "maximum chain" stays to a      |
|       reasonable number, and running with twice as many nodes as threads generally   |
|       produces no wait for stolen nodes. Anyway, as the backoff process works by     |
|       counting atomic operations, it also protects against such unlikely situation.  |
|     IMPORTANT NOTE: you must consider the node structure as opaque and don't play    |
|       with the pointers in it, because push delayed to helpers relies on the fact    |
|       that no one touches the pNext pointer of the node (the one we must CAS) as     |
|       long as there are threads on their critical sections.                          |
| -4) DCAS (Double Word CAS)                                                           |
|     When available, we do simply add an age counter on the "pop" to avoid the ABA    |
|     issue there. Thus it is very straightforward, and certainly the best choice in   |
|     most case. It beats the CAS by 25% when no contention, is on par on medium       |
|     contention, and better again on higher contention. Although missed 128 Bytes     |
|     DCAS are expensive, the simple CAS algorithm has to deal with helpers which are  |
|     extremely slow and slow down the whole process.                                  |
|     DCAS is also lock-free (and same, not wait-free), and immune to stalls. As we    |
|     have no need for "helpers", a stalled thread won't "steal" an element of queue!  |
|     Note a) of CAS applies also to this DCAS agorithm.                               |
|                                                                                      |
|                                                                                      |
| Typical usage:                                                                       |
|===============                                                                       |
| IMPORTANT: the typical use is letting the module do the allocations. Using static    |
|     memory or user-allocated is discouraged because, as the structures are opaque    |
|     they might be some discrepancy in what the usr allocates and what the module     |
|     is expecting. They might also be alignment issue. If anyway you want to do so    |
|     the most important thing is to make sure the buffers you use are zeroed before   |
|     their first use with the module. Look at the example for more information.       |
|     Here we describe only the typical recommended use, letting the module do         |
|     allocations.                                                                     |
| -1) Setup a head for your LIFO queue.                                                |
|     You simply call lifo_setup_head providing the address where the pointer to head  |
|     must be returned, and the mode of the queue (NONE, LOCK, CAS, DCAS). You can     |
|     also provide LIFO_DEF_MODE for the mode parameter, the module will use the       |
|     default mode that was specified during compilation.                              |
|     Check the return code for ELIFO_OK, and you are good to work with your queue.    |
| -2) Nodes creation                                                                   |
|     Same as head, use lifo_setup_node providing the address where the node pointer   |
|     must be returned, and the size of your user structure.                           |
|     Check the return code for ELIFO_OK, and you can now store data in you user       |
|     structure.                                                                       |
|     You repeat these steps for every new node you want to allocate.                  |
| -3) Pushing and Popping.                                                             |
|     Now, we assume, as the pattern says, that you have been allocating resources     |
|     (the nodes) and you are done working with a resource, so you will just push it   |
|     on the queue for subsequent use. You juste need to call: lifo_push with the head |
|     and your node's addresses. Once you want a resource again, do the opposite and   |
|     call lifo_pop with the head address, and the address where you'll want the node  |
|     pointer returned. If there are no nodes to pop, you will get a NULL pointer.     |
|     These functions ALWAYS work, so they do NOT return errors (always ELIFO_OK),     |
|     unless the checks are activated at compile time. If so they will check for NULL  |
|     and unaligned pointers and return errors should that happen.                     |
| -4) Cleaning the queue.                                                              |
|     Assuming you have done all steps from 1 to 3, once you are done with the queue   |
|     and made sure all the threads are joined, or at least don't keep any references  |
|     to head or nodes, you just need to call lifo_cleanup_head, supplying the head    |
|     pointer, and a boolean specifying if you want nodes be cleaned-up too (you       |
|     should the say *true* for that if you did step 2 and didn't cleanup the nodes).  |
|     This function will take care of all de-allocations.                              |
|     Test anyway for ELIFO_OK. Some error conditions can happen if activity on the    |
|     queue is still detected (it shouldn't, unless you forgot somme running thread!)  |
|                                                                                      |
|--------------------------------------------------------------------------------------|
| TODO:                                                                                |
|======                                                                                |
|     Sorry, this is quite a big TODO!                                                 |
|     It is completely untested on non-Intel/AMD processors, as I do not have the      |
|     equipment to test that. It should at least been tested/corrected on ARM which    |
|     are more and more popular.                                                       |
|                                                                                      |
|     x86(32/64) processors make things easy because of their strict memory ordering.  |
|     For ARM, I suspect that it could 'almost' work out-of the box, assuming that     |
|     (to be checked!) any atomic operation triggers a full barrier. In fact the only  |
|     thing we store with no barrier is the pointer to the next node inside a node we  |
|     are going to push. But as the push is only successful when the CAS that is       |
|     following works, we would have a full barrier before any thread (other than      |
|     helpers, but that is taken care of in the helping process) uses the value to pop.|
|     Most probably, the only thing that could fail for ARM is setting stats and       |
|     threshold at the same time. That would require an explicit release barrier to    |
|     avoid one of the operations cancels the other.                                   |
|     It is also unknown (for me) whether ARM has the equivalent of DCAS. That part is |
|     (pessimistically) safe by default, as DCAS is activated only for Intel target.   |
|                                                                                      |
|     We also did an assumption that is safe is almost (but not all) modern processors |
|     which is that any load and store on a word aligned word is atomic. Thus, for     |
|     simplicity reasons, we didn't overload the already complicated code with         |
|     atomic_load/atomic_store functions.                                              |
|     Should we need that, a revision to all loads and store would have to be done!    |
|                                                                                      |
|     NEW(0.2.0): ARM testing. I could test successfully on my Synology DS413j...      |
|       but the test is inconclusive because it has a Marvell Kirkwood mv6282          |
|       processor (ARM v5te), which has a single core. So, obviously the memory is     |
|       always oredered on one core! Furthermore, GCC reports that there is no CAS     |
|       on this processor (don't know if it is correct, or just that GCC has not yet   |
|       implemented CAS on this type of ARM processor), so the library fallbacks to    |
|       using LOCKS (mutexes) which obviously works!                                   |
|       Remarks: my Synology has an old GCC 4.2.3 who does not understand -std=gnu1x   |
|         nor -mcx16. So, these 2 options must be removed (anyway mcx16 is for Intel). |
|         Also, the linker does not find __sync_add_and_fetch_4 that is used within    |
|         statistics. I didn't investigate more about that, and simply compiled        |
|         without stats!                                                               |                                                                   
\=====================================================================================*/


/*=====================================================================================\
| Includes, macros, globals and function definitions.                                  |
\=====================================================================================*/

#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <limits.h>
#include <errno.h>
#include <pthread.h>

#include "lifo.h"


#warning "*******************************************************"
#warning "The few warnings issued by #warning below are 'normal'."
#warning "They show compile options used.                        "


#ifdef CHECK        /* We make sure the value is 0 or 1 */ 
  #if CHECK
    #undef  CHECK
    #define CHECK 1
  #else
    #undef  CHECK
    #define CHECK 0
  #endif
#else
  #define CHECK 0   /* And default is 0 if not defined  */
#endif

#if CHECK == 0
  #warning "Pointer checks disabled (default).                     "
#else
  #warning "Pointer checks are enabled by compile option.          "
#endif


#ifdef STATS            /* We make sure the value is 0 or 1 */ 
  #if STATS
    #undef  STATS
    #define STATS 1
  #else
    #undef  STATS
    #define STATS 0
  #endif
#else
  #define STATS 0       /* And default is 0 if not defined  */
#endif

#if STATS == 0
  #warning "Statistics are disabled (default).                     "
#else
  #warning "Statistics are enabled.                                "
#endif

#undef LIFO_CAS_OK
#if __WORDSIZE == 32
  #ifdef __GCC_HAVE_SYNC_COMPARE_AND_SWAP_4
    #define LIFO_CAS_OK
  #else
    #warning "Your target processor does not have CAS on 4 bytes     "
  #endif
  #define LIFO_32
  #define LIFO_64 0
#elif __WORDSIZE == 64
  #ifdef __GCC_HAVE_SYNC_COMPARE_AND_SWAP_8
    #define LIFO_CAS_OK
  #else
    #warning "Your target processor does not have CAS on 8 bytes     "
  #endif
  #define LIFO_64 1
#else
  #error "This library is tested only for 32 or 64bits pointer   "
#endif


#ifdef CAS_ENABLED  /* If not defined, we don't change LIFO_CAS_OK */
  #if CAS_ENABLED == 0
    #undef  CAS_ENABLED    /* We want to be sure it's 0 or 1 */
    #define CAS_ENABLED 0  /* as 0 can be 'not integer'      */
    #ifdef LIFO_CAS_OK
      #warning "CAS is disabled by compile option.                     "
    #else
      #warning "CAS is disabled by compile option...                   "
      #warning "     ... but was already disabled by gcc.              "
    #endif
  #else /* CAS_ENABLED != 0 here */
    #undef  CAS_ENABLED    /* We want to be sure it's 0 or 1 */
    #ifdef LIFO_CAS_OK
      #define CAS_ENABLED 1  /* so if not 0, we set it to 1    */
    #else
      #warning "Although CAS was enabled, it is forced to disabled as  "
      #warning "  gcc reports your target processor do not have CAS.   "
      #define CAS_ENABLED 0
    #endif
  #endif
#else /* CAS_ENABLED is undef here */
  #ifdef LIFO_CAS_OK      /* We set CAS_ENABLED according to LIFO_CAS_OK */
    #define CAS_ENABLED 1
  #else
    #define CAS_ENABLED 0
  #endif
#endif

#if CAS_ENABLED == 0
  #warning "CAS is off: ignoring DEF_MODE, default to 2 (LIFO_LOCK)"
  #undef  DEF_MODE
  #define DEF_MODE   2
  #warning "CAS is off: ignoring THRESHOLD, default to -1: disabled"
  #undef  THRESHOLD
  #define THRESHOLD -1
#else
  #ifndef DEF_MODE
    #define DEF_MODE 2
  #endif
#endif

#ifdef THRESHOLD         /* We make sure the value is integer*/ 
  #if THRESHOLD == 0
    #undef  THRESHOLD
    #define THRESHOLD 0
  #endif
  #if CAS_ENABLED == 0
    #warning "As CAS is off, no need of backoff: THRESHOLD set to -1 "
    #undef  THRESHOLD
    #define THRESHOLD 0
    #define BACKOFF 0
  #else
    #if THRESHOLD == -1
      #undef  THRESHOLD
      #define THRESHOLD 0
      #define BACKOFF 0
    #else
      #define BACKOFF 1
    #endif
  #endif
#else
  #define BACKOFF   0   /* And default is no Backoff if not defined  */
  #define THRESHOLD 0
#endif

#if BACKOFF == 0
  #warning "Backoff is disabled (default).                         "
#else
  #warning "Backoff is enabled                                     "
  #if THRESHOLD > 16
    #warning "THRESHOLD shouldn't be that big, please check!..       "
  #endif
#endif

#ifndef DEF_MODE
  #define DEF_MODE 4  /* DCAS by default */
#endif

#if DEF_MODE == 1
  #undef  DEF_MODE
  #define DEF_MODE LIFO_NONE
  #warning "Default mode is LIFO_NONE                              "
#else
 #if DEF_MODE == 2
  #warning "Default mode is LIFO_LOCK                              "
  #undef  DEF_MODE
  #define DEF_MODE LIFO_LOCK
 #else
  #if DEF_MODE == 3
    #warning "Default mode is LIFO_CAS                               "
    #undef  DEF_MODE
    #define DEF_MODE LIFO_CAS
  #else
    #if DEF_MODE == 4
      #warning "Default mode is LIFO_DCAS                              "
      #undef  DEF_MODE
      #define DEF_MODE LIFO_DCAS
    #else
      #warning "DEF_MODE must be in [1-4]. Defaulting to 2 (LIFO_LOCK) "
      #undef  DEF_MODE
      #define DEF_MODE LIFO_LOCK
    #endif  /* DEF_MODE == 4  */
  #endif  /* DEF_MODE == 3  */
 #endif  /* DEF_MODE == 2  */
#endif  /* DEF_MODE == 1  */

#ifdef LIFO_32
  #warning "We assume 32bits environment                           "
#else
  #warning "This is 64bits environment                             "
#endif

#if defined(__x86_64) || defined(__x86_64__) || \
    defined(__amd64)  || defined(__amd64__)  || \
    defined(__ia64__) || defined(__IA64__)   || \
    defined(i386)     || defined(__i386__)   || defined(__i486__) || \
    defined(__i686)   || defined(__pentium4) || defined(__athlon) || \
    defined(__pentium__)
  #warning "This is a compilation for Intel or compatible target.  "
  #define LIFO_TARGET_INTEL 1

  #if LEVEL1_DCACHE_LINESIZE != 64
    #warning "ATTENTION! For Intel targets cache_size should be 64.  "
  #endif

#else
  #define LIFO_TARGET_INTEL 0
  #warning "We did NOT detect Intel target, DCAS will be disabled. "
#endif

#warning "This is the end of warnings issued by #warning         "
#warning "All subsequent warnings (should be none!) are 'real'   "
#warning "*******************************************************"


const int lifo_features=  ( BACKOFF                ) +
                          ( CHECK         << 1 ) +
                          ( CAS_ENABLED       << 2 ) +
                          ( LIFO_TARGET_INTEL << 3 ) +
                          ( LIFO_64           << 4 ) ;
const int lifo_cache_align= LEVEL1_DCACHE_LINESIZE;
const struct lifo_options lifo_defaults = { THRESHOLD, STATS, 0, DEF_MODE };

#define F_INIT_DONE   1
#define F_ALLOC_HEAD  2
#define F_ALLOC_STATS 4

#if STATS
  #define FSTATS  , bool fStats
  #define FSTATS_UNUSED  FSTATS __attribute__ ((unused))
#else
  #define FSTATS
  #define FSTATS_UNUSED
#endif

#define UNUSED_PARAM(param) (void)param

                            /*=========================================================\
                            |  We split the structure into variable parts and (almost) |
                            |  constant parts, so that we get less cache traffic.      |
                            |  Thus, the function pointers are on a new cacheline.     |
                            |  Note that on 64bits Intel, as the 'variable' part of    |
                            |  the struct is 64bits long, the alignment has no effect. |
                            |  On 32bits archi the alignment will pad some bytes.      |
                            |  Thus, the total struct length, for Intel procs, is:     |
                            |  104bytes in x64, and 88bytes for 32bits procs.          |
                            \=========================================================*/
struct lifo_head
  {
    struct lifo_node    *pFirst;
    long                 age;
    unsigned long        backoff;
    pthread_mutex_t      mutex;
    int                (*push)(struct lifo_head *, struct lifo_node *  FSTATS) 
                                   __attribute__ ((aligned (LEVEL1_DCACHE_LINESIZE)));
    int                (*lpop)(struct lifo_head *, void **             FSTATS);
    int                (*mpop)(struct lifo_head *, void **             FSTATS);
    struct lifo_options  options;
#if STATS == 1
    unsigned long       *pStats;
#endif
  };

struct lifo_node
  {
#if CAS_ENABLED
    long              count;
#endif
    struct lifo_node *pNext;
  };
const int lifo_sizeof_node= sizeof(struct lifo_node);



/*=====================================================================================\
| IMPORTANT NOTE: msg_node is shorter than lifo_node as we do not need count for CAS.  |
|                 This allows us to allocate fewer bytes for msg + user structure.     |
|                 But as we use lifo functions for the push part of msg routine (they  |
|                 do what we need: code duplication is useless!), we must be careful   |
|                 that pNext is the LAST element in lifo_node so that we always get    |
|                 it correctly in the lifo function, whatever node was allocated. That |
|                 is because we substract the structure size from the pointer that is  |
|                 passed to the function then access the pNext, thus if pNext at the   |
|                 same position in reference to the end of our structure, it is fine.  |
| ALSO NOTE: as we do no operations on the nodes in the msg_pop functions we do not    |
|            stricly need alignment. But we still check that during push (lifo used)   |
|            thus nodes still need pointer size alignment... which is anyway the       |
|            default behaviour of malloc, and a good practice for performance sake!    |
|            If this is really disturbing for you, and you crave for unaligned pointers|
|            you can still use the library with checks disabled... which makes sense   |
|            because the only case you would want to do that is when you really want   |
|            space optimization, thus a more compact library is suitable.              |
\=====================================================================================*/
struct msg_node
  {
    struct msg_node *pNext;
  };

#define PLIFONODE_FROM_PUSERNODE(pUserNode)  ( ((struct lifo_node *)pUserNode) - 1 )
#define  PMSGNODE_FROM_PUSERNODE(pUserNode)  ( ((struct msg_node * )pUserNode) - 1 )



static int lifo_push_N     (struct lifo_head  *pHead,
                            struct lifo_node  *pNode
                            FSTATS                   );
static int lifo_pop_N      (struct lifo_head  *pHead,
                            void             **ppNode
                            FSTATS                   );

static int msg_pop_N       (struct msg_head   *pHead,
                            void             **ppNode
                            FSTATS                   );

static int lifo_push_L     (struct lifo_head  *pHead,
                            struct lifo_node  *pNode
                            FSTATS                   );
static int lifo_pop_L      (struct lifo_head  *pHead,
                            void             **ppNode
                            FSTATS                   );

static int msg_pop_L       (struct msg_head   *pHead,
                            void             **ppNode
                            FSTATS                   );

#if CAS_ENABLED
static int lifo_push_C     (struct lifo_head  *pHead,
                            struct lifo_node  *pNode
                            FSTATS                   );
static int lifo_pop_C      (struct lifo_head  *pHead,
                            void             **ppNode
                            FSTATS                   );
static int lifo_push_D     (struct lifo_head  *pHead,
                            struct lifo_node  *pNode
                            FSTATS                   );
static int lifo_pop_D      (struct lifo_head  *pHead,
                            void             **ppNode
                            FSTATS                   );

static int msg_pop_C       (struct msg_head   *pHead,
                            void             **ppNode
                            FSTATS                   );
#endif


#if CHECK
  #define ptr_check_null(p)       if ( p == NULL )  return ELIFO_NULL
  #define head_check_aligned(p)   if ( \
                                       ( ((uintptr_t) p) % LEVEL1_DCACHE_LINESIZE ) \
                                     )              return ELIFO_UNALIGNED
  #define node_check_aligned(p,a) if ( \
                                      ( ((uintptr_t) p) % a ) \
                                     ) \
                                                    return ELIFO_UNALIGNED
  #define check_init(pHead)       if ( (pHead->options.flags & F_INIT_DONE) == 0) \
                                    return ELIFO_UNINIT 
  #define full_check(pHead)       ptr_check_null(pHead);    \
                                  head_check_aligned(pHead);\
                                  check_init(pHead)
                                  
#else
  #define ptr_check_null(p)
  #define head_check_aligned(p)
  #define node_check_aligned(p,a)
  #define check_init(pHead)
  #define full_check(pHead)
#endif


/*=====================================================================================\
| Specific Intel i386/amd64 assembly function to test DCAS feature existence           |
\=====================================================================================*/

/*=====================================================================================\
| This function checks whether cmxchg16b (or 8b on 32bits) is available, so that we    |
|   can safely DCAS. As it is specific to Intel, if a non-intel target is detected     |
|   we just return false.                                                              |
| Note: we must save register (execpt AX which is anyway used for return values) as    |
|  when gcc is called with -O3, it will uses registers in an agressive manner, and     |
|  things gets mixed up with our assembly code, resulting in core dumps or unpredic-   |
|  table results. Thus we save and restore bx,cx,dx that are set by the CPUID calls.   |
|  For the same reason, we declare this function noinline, otherwise -O3 inlines it    |
|  and mixes up the assembly code which results in core dumps.                         |
\=====================================================================================*/
#if CAS_ENABLED
static bool lifo_has_dcas( void ) __attribute__((noinline));
static bool lifo_has_dcas( void )
{
#if LIFO_TARGET_INTEL == 1
  unsigned int veax;

#ifdef LIFO_32
  asm ("push %ebx");
  asm ("push %ecx");
  asm ("push %edx");
#else
  asm ("push %rbx");
  asm ("push %rcx");
  asm ("push %rdx");
#endif /*LIFO_32*/
  asm ("mov $0, %eax");
  asm ("mov $0, %ecx");
  asm ("cpuid");
  asm ("movl %%eax, %0": "=g" (veax) );
  if (veax)
    {
      asm ("mov $1, %eax");
      asm ("mov $0, %ecx");
      asm ("cpuid");

#ifdef LIFO_32  /* We want to know if cmpxchg8b is supported  */
      unsigned int vedx;
      asm ("movl %%edx, %0": "=g" (vedx) );

      if (vedx & 0x00000100)   /* cmpxchg8b is bit 8 of edx   */
        {
          asm ("pop %edx");
          asm ("pop %ecx");
          asm ("pop %ebx");
          return true;
        }
#else
      unsigned int vecx;
      asm ("movl %%ecx, %0": "=g" (vecx) );
      if (vecx & 0x00002000)   /* cmpxchg16b is bit 13 of ecx */
        {
          asm ("pop %rdx");
          asm ("pop %rcx");
          asm ("pop %rbx");
          return true;
        }
#endif /*LIFO_32*/
    }
#endif /*LIFO_TARGET_INTEL*/

#ifdef LIFO_32
  asm ("pop %edx");
  asm ("pop %ecx");
  asm ("pop %ebx");
#else
  asm ("pop %rdx");
  asm ("pop %rcx");
  asm ("pop %rbx");
#endif /*LIFO_32*/
  return false;
}
#endif /*CAS_ENABLED*/



/*=====================================================================================\
| Initialisation and miscellaneous functions                                           |
| ------------------------------------------                                           |
| As a general rule, when statistics or backoff are disabled at compile time, the      |
| related function do nothing but returning ELIFO_UNAVAIL                              |
\=====================================================================================*/

/*=====================================================================================\
| You will find here utility functions useful to:                                      |
| - allocate stuff                                                                     |
| - initialise stuff                                                                   |
| - get and set options for a queue (stats, backoff threshold, mode -get only-)        |
| - get statistics                                                                     |
| - free stuff                                                                         |
| Functions are grouped by object they manipulate                                      |
\=====================================================================================*/


/*=====================================================================================\
| HEAD                                                                                 |
| - lifo_setup_head:   allocates and initializes the head structure.                   |
| - msg_setup_head:    (and same for msg)                                              |
| - lifo_init_head:    initializes the head structure. This function can be used       |
|                      directly if the caller already has a head structure (allocated  |
|                      or static). In this case, the caller must provide a zero        |
|                      initialized structure.                                          |
| - msg_init_head:     (and same for msg)                                              |
| - lifo_cleanup_head: cleansup the queue, along with statistics if necessary, then    |
|                      frees memory (if it was allocated by setup, otherwise if the    |
|                      user called directly lifo_init_head, he must properly free      |
|                      memory when necessary)                                          |
| - msg_cleanup_head:  (and same for msg)                                              |
\=====================================================================================*/

static int 
common_setup_head( struct lifo_head   **ppHead,
                          lifo_mode     mode  ,
                   int                (*init)(struct lifo_head *, lifo_mode  mode )
                 )
{
  int err;
  ptr_check_null(ppHead);

  err= posix_memalign( (void **)ppHead         , 
                       LEVEL1_DCACHE_LINESIZE  ,
                       sizeof(struct lifo_head)
                     );
  if (err != 0)
    {
      return err;
    }

  (*ppHead)->options.flags= F_ALLOC_HEAD;

  err= init( *ppHead, mode );

  if (err != ELIFO_OK)
    {
      free( *ppHead );
      *ppHead= NULL;
    }

  return err;
}


int lifo_setup_head( struct lifo_head   **ppHead,
                            lifo_mode     mode   )
{
  return common_setup_head( ppHead, mode, lifo_init_head );
}

int msg_setup_head( struct msg_head   **ppHead,
                           msg_mode     mode   )
{
  return common_setup_head( ppHead, mode, msg_init_head );
}


#if BACKOFF
  #define BACKOFF_INIT(pHead)  pHead->backoff= 0;                                    \
                               if ( ( (pHead->options.flags & F_INIT_DONE) == 0 ) || \
                                       pHead->options.mode == LIFO_DCAS           || \
                                       pHead->options.mode == LIFO_CAS               \
                                  )                                                  \
                                 pHead->options.threshold= lifo_defaults.threshold;  \
                               pthread_mutex_init( &(pHead->mutex), NULL)
#else
  #define BACKOFF_INIT(pHead)
#endif

static int
common_init_head( struct lifo_head *pHead,
                         lifo_mode  mode , 
                  bool              lifo  )
{
  pHead->pFirst = NULL;

  switch ( mode )
    {
      case LIFO_DCAS :
#if CAS_ENABLED
                        if ( lifo && lifo_has_dcas() )
                          {
                            head_check_aligned(pHead);
                            pHead->push= lifo_push_D;
                            pHead->lpop= lifo_pop_D;
                            pHead->age = 0;
                            BACKOFF_INIT(pHead);
                            pHead->options.mode= LIFO_DCAS;
                            break;
                          }
#else
                        UNUSED_PARAM(lifo);
#endif
      case LIFO_CAS  :
#if CAS_ENABLED
                        head_check_aligned(pHead);
                        BACKOFF_INIT(pHead);
                        pHead->options.mode= LIFO_CAS;
                        break;
#endif
      case LIFO_LOCK :
                        pHead->push= lifo_push_L;
                        pthread_mutex_init( &(pHead->mutex), NULL);
                        pHead->options.mode= LIFO_LOCK;
                        break;
      case LIFO_NONE :
                        pHead->push= lifo_push_N;
                        pHead->options.mode= LIFO_NONE;
                        break;
      default        :  
                        pHead->push= NULL;
                        pHead->lpop= NULL;
                        pHead->mpop= NULL;
                        pHead->options.flags &= ~F_INIT_DONE;
                        pHead->options.mode= LIFO_ERROR;
                        return ELIFO_MODE_ERR;
   }
  pHead->options.flags |= F_INIT_DONE;
  return ELIFO_OK;
}


#if STATS
  #define PRE_INIT_STATS(popFct) \
        if (   (pHead->options.flags & F_ALLOC_STATS) &&  \
             (                                            \
                mode != pHead->options.mode ||            \
                popFct != NULL                            \
             )                                            \
           )                                              \
          {                                               \
            return ELIFO_MODE_ERR;                        \
          }                                               \
      }                                                   \
    else                                                  \
      {                                                   \
        pHead->pStats       = NULL;                       \
        pHead->options.stats= false;
#else
  #define PRE_INIT_STATS(popFct)
#endif

#define PRE_INIT_HEAD(popFct) \
    ptr_check_null(pHead);                                \
    head_check_aligned(pHead);                            \
    if ( pHead->options.flags & F_INIT_DONE )             \
      {                                                   \
        if ( pHead->pFirst != NULL )                      \
          {                                               \
            return ELIFO_BUSY;                            \
          }                                               \
        PRE_INIT_STATS(popFct);                           \
      }                                                   \
    popFct= NULL;                                         \
    if ( mode == LIFO_DEF_MODE )                          \
      {                                                   \
        mode= DEF_MODE;                                   \
      }

int lifo_init_head( struct lifo_head *pHead,
                           lifo_mode  mode  )
{
  PRE_INIT_HEAD(pHead->mpop);

  switch ( mode )
    {
      case LIFO_DCAS :
#if CAS_ENABLED
                        break;
#endif
      case LIFO_CAS  :
#if CAS_ENABLED
                        pHead->push= lifo_push_C;
                        pHead->lpop= lifo_pop_C;
                        break;
#endif
      case LIFO_LOCK :
                        pHead->lpop= lifo_pop_L;
                        break;
      case LIFO_NONE :
                        pHead->lpop= lifo_pop_N;
                        break;
      default        :  
                        break;
    }
  return common_init_head( pHead, mode, true );
}

int msg_init_head( struct msg_head *pHead,
                          msg_mode  mode  )
{
  PRE_INIT_HEAD(pHead->lpop);

  switch ( mode )
    {
      case LIFO_DCAS :
      case LIFO_CAS  :
#if CAS_ENABLED
                        pHead->push= lifo_push_D;
                        pHead->mpop= msg_pop_C;
                        break;
#endif
      case LIFO_LOCK :
                        pHead->mpop= msg_pop_L;
                        break;
      case LIFO_NONE :  
                        pHead->mpop= msg_pop_N;
                        break;
      default        :
                        break;
   }
  return common_init_head( pHead, mode, false );
}


static void common_cleanup_stats(struct lifo_head *pHead );

static int common_cleanup_head( struct lifo_head *pHead )
{
  if ( pHead->pFirst != NULL )
    {
      return ELIFO_BUSY;
    }
  common_cleanup_stats(pHead);
  if ( pHead->options.flags & F_ALLOC_HEAD )
    {
      free( pHead );
    }
  else
    {
      pHead->options.flags &= ~F_INIT_DONE;
    }
  return ELIFO_OK;
}

int lifo_cleanup_head( struct lifo_head *pHead, bool fFreeNodes )
{
  void *pNode;

  ptr_check_null(pHead);
  check_init(pHead);

  if ( fFreeNodes )
    {
      while(pHead->pFirst != NULL)
        {
           lifo_pop( pHead, &pNode );
           if ( pNode == NULL )
             {
               return ELIFO_BUSY;
             }
           else
             {
               if ( lifo_cleanup_node( pNode ) != ELIFO_OK )
                 {
                   return ELIFO_BUSY;
                 }
             }
        }
    }

  return common_cleanup_head( pHead );
}


int msg_cleanup_head( struct msg_head *pHead, bool fFreeNodes )
{
  void *pNode, *pFree;

  ptr_check_null(pHead);
  check_init(pHead);

  if ( fFreeNodes )
    {
      msg_pop( pHead, &pNode );
      while ( pNode != NULL )
        {
          pFree= pNode;
          pNode= msg_next( pNode );
          msg_cleanup_node( pFree );
        }
    }

  return common_cleanup_head( pHead );
}

/*=====================================================================================\
| STATS                                                                                |
| - lifo_setup_stats:  allocates and initializes the stats structure.                  |
| - lifo_init_stats:   initializes the stats structure. Same as the head, can be used  |
|                      directly if the caller already has a head structure. Be aware   |
|                      the structure size is very different according to the mode.     |
| - lifo_cleanup_stats:cleansup the stats and removes them from the queue.             |
\=====================================================================================*/

#if STATS == 1
static const unsigned short lifo_stats_limits[]= { 0,
                                                   LIFO_STAT_LIMIT_NONE,
                                                   LIFO_STAT_LIMIT_LOCK,
                                                   LIFO_STAT_LIMIT_CAS ,
                                                   LIFO_STAT_LIMIT_DCAS
                                                 };
static const unsigned short  msg_stats_limits[]= { 0,
                                                   LIFO_STAT_LIMIT_NONE,
                                                   LIFO_STAT_LIMIT_LOCK,
                                                   MSG_STAT_LIMIT_CAS
                                                 };
#else
  #define lifo_stats_limits NULL
  #define  msg_stats_limits NULL
#endif

static int common_init_stats ( struct lifo_head *pHead , 
                               unsigned long    *pStats,
                               const unsigned short   *pLimits  );


static int common_setup_stats  ( struct lifo_head     *pHead, 
                                 const unsigned short *pLimits  )
{
#if STATS == 0

  UNUSED_PARAM(pHead);
  UNUSED_PARAM(pLimits);
  return ELIFO_UNAVAIL;
#else
  unsigned long *pStats;
  full_check(pHead);

                                       /* For 'NONE' mode, the Mutex is unused and    */
                                       /* big enough to fit our 3 stat counters, so   */
                                       /* we don't need another allocation            */
  if ( pHead->options.mode == LIFO_NONE )
    {
      pStats= (unsigned long *)(&(pHead->mutex));
      pHead->options.flags &= ~F_ALLOC_STATS;
    }
  else
    {
      int err;

      err= posix_memalign( (void **)(&pStats)   ,
                            LEVEL1_DCACHE_LINESIZE        ,
                            pLimits[pHead->options.mode] * sizeof(unsigned long)
                         );
      if (err != 0)
        {
          return err;
        }

      pHead->options.flags |= F_ALLOC_STATS;
    }

  common_init_stats( pHead, pStats, pLimits );

  return ELIFO_OK;
#endif /* STATS */
}

int lifo_setup_stats  ( struct lifo_head *pHead )
{
  return common_setup_stats( pHead, lifo_stats_limits );
}

int msg_setup_stats  ( struct msg_head *pHead )
{
  return common_setup_stats( pHead, msg_stats_limits );
}


static int common_init_stats ( struct lifo_head *pHead , 
                               unsigned long    *pStats,
                               const unsigned short   *pLimits  )
{
#if STATS == 0
  UNUSED_PARAM(pHead);
  UNUSED_PARAM(pStats);
  UNUSED_PARAM(pLimits);
  return ELIFO_UNAVAIL;
#else
  unsigned int i;
  full_check(pHead);
  if (pHead->options.mode != LIFO_NONE)
    {
      head_check_aligned(pStats);
    }

  if ( pStats == NULL )
    {
      if ( pHead->pStats == NULL )
        {
          return ELIFO_NULL;
        }
    }
  else
    {
      pHead->pStats= pStats;
    }

  for (i=0 ; i< pLimits[pHead->options.mode]; i++)
    {
      pHead->pStats[i]= 0;
    }

  pHead->options.stats= true;

  return ELIFO_OK;
#endif /* STATS */
}

int lifo_init_stats ( struct lifo_head *pHead, unsigned long *pStats  )
{
  return common_init_stats( pHead, pStats, lifo_stats_limits );
}

int msg_init_stats ( struct msg_head *pHead, unsigned long *pStats  )
{
  return common_init_stats( pHead, pStats, msg_stats_limits );
}


static void common_cleanup_stats( struct lifo_head *pHead )
{
#if STATS == 0
  UNUSED_PARAM(pHead);
#else
  pHead->options.stats= false;
  if ( pHead->options.flags & F_ALLOC_STATS )
    {
      free( pHead->pStats );
      pHead->options.flags &= ~F_ALLOC_STATS;
    }
  pHead->pStats=NULL;
#endif /* STATS */
}

int lifo_cleanup_stats( struct lifo_head *pHead )
{
#if STATS == 0
  UNUSED_PARAM(pHead);
  return ELIFO_UNAVAIL;
#else
  full_check(pHead);
  
  common_cleanup_stats(pHead);
  return ELIFO_OK;
#endif /* STATS */
}

/*=====================================================================================\
|  This function returns the statistic counter that is passed.                         |
|  If the counter passed is not available (current mode does not have this counter or  |
|  stats disabled at compile time) then the function returns ULONG_MAX.                |
\=====================================================================================*/

int lifo_stat ( struct lifo_head    *pHead     ,
                unsigned int         statIndex ,
                unsigned long       *pStatCntr )
{
#if STATS==0
  UNUSED_PARAM(pHead);
  UNUSED_PARAM(statIndex);
  UNUSED_PARAM(pStatCntr);
#else
  const unsigned short *pLimits;
  full_check(pHead);
  ptr_check_null(pStatCntr);

  pLimits= (pHead->mpop == NULL) ? lifo_stats_limits : msg_stats_limits;
  if ( pHead->pStats == NULL )
    {
      return ELIFO_UNINIT;
    }
  if ( statIndex < pLimits[pHead->options.mode]  )
    {
      *pStatCntr= pHead->pStats[statIndex];
      return ELIFO_OK;
    }
#endif
  return ELIFO_UNAVAIL;
}



/*=====================================================================================\
| NODES                                                                                |
| - lifo_setup_node:   allocates and initializes the node structure.                   |
| - msg_setup_node:    (same for msg)                                                  |
| - lifo_init_node:    initializes the stats structure. Same as the head, can be used  |
|                      directly if the caller already has a head structure.            |
| - msg_init_node:     There is no such function. It is defined as a macro replaced    |
|                      by ELIFO_OK as msg_nodes don't need any initialization.         |
| - lifo_cleanup_node: cleansup the nodes (mainly free the memory).                    |
| - msg_cleanup_node:  (same for msg)                                                  |
|                                                                                      |
| The allocation is aligned to fit both our internal node structures and the user data |
| in a single cache line (if it exceeds the cache line size, we cache-line-size align) |
\=====================================================================================*/


static int
common_setup_node( void **ppUserNode, size_t size, size_t offset, bool fInit )
{
  int error;
  size_t alignment;
  ptr_check_null(ppUserNode);

  for( alignment= LEVEL1_DCACHE_LINESIZE; offset + size <= alignment/2; alignment/= 2 );

  error= posix_memalign( ppUserNode,
                         alignment ,
                         offset + size );
  if (error == 0)
    {
      *ppUserNode= ((char *)*ppUserNode ) + offset;
      if (fInit)
        {
          lifo_init_node( *ppUserNode );
        }
    }
  return error;
}

int lifo_setup_node( void **ppUserNode, size_t size )
{
  return common_setup_node( ppUserNode, size, sizeof(struct lifo_node), true);
}

int msg_setup_node( void **ppUserNode, size_t size )
{
  return common_setup_node( ppUserNode, size, sizeof(struct msg_node), false);
}


int lifo_init_node ( struct lifo_node *pUserNode )
{
#if CAS_ENABLED
  ptr_check_null(pUserNode);
  PLIFONODE_FROM_PUSERNODE( pUserNode )->count = 0;
#else
  UNUSED_PARAM(pUserNode);
#endif
  return ELIFO_OK;
}

int lifo_cleanup_node( void *pUserNode )
{
#if CAS_ENABLED
  if ( PLIFONODE_FROM_PUSERNODE( pUserNode )->count != 0 )
    {
      return ELIFO_BUSY;
    }
#endif
  free( PLIFONODE_FROM_PUSERNODE( pUserNode ) );
  return ELIFO_OK;
}

int msg_cleanup_node( void *pUserNode )
{
  free( PMSGNODE_FROM_PUSERNODE( pUserNode ) );
  return ELIFO_OK;
}


/*=====================================================================================\
| OPTIONS Get & Set                                                                    |
| - Get only: the mode used by the queue can only be read. It is set by lifo_init-head |
|             and then is readonly.                                                    |
| - Get & Set: Threshold and statictics can be read and changed... provided they were  |
|             included at compile time. If not the functions return ELIFO_UNAVAIL      |
\=====================================================================================*/


int lifo_get_mode( struct lifo_head *pHead, lifo_mode *pMode )
{
  full_check(pHead);
  ptr_check_null(pMode);

  *pMode= pHead->options.mode;
  return ELIFO_OK;
}


int lifo_get_threshold( struct lifo_head    *pHead     ,
                        unsigned short int  *pThreshold  )
{
#if BACKOFF
  full_check(pHead);
  ptr_check_null(pThreshold);

  *pThreshold= pHead->options.threshold;
  return ELIFO_OK;
#else
  UNUSED_PARAM(pHead);
  UNUSED_PARAM(pThreshold);
  return ELIFO_UNAVAIL;
#endif
}

int lifo_set_threshold( struct lifo_head    *pHead     ,
                        unsigned short int   threshold  )
{
#if BACKOFF
  full_check(pHead);

  pHead->options.threshold= threshold;

  return ELIFO_OK;
#else
  UNUSED_PARAM(pHead);
  UNUSED_PARAM(threshold);
  return ELIFO_UNAVAIL;
#endif
}

int lifo_get_stats( struct lifo_head *pHead, bool *pStats )
{
#if STATS
  full_check(pHead);
  ptr_check_null(pStats);

  *pStats= pHead->options.stats;
  return ELIFO_OK;

#else
  UNUSED_PARAM(pHead);
  UNUSED_PARAM(pStats);
  return ELIFO_UNAVAIL;
#endif
}

int lifo_set_stats( struct lifo_head *pHead, bool stats )
{
#if STATS
  full_check(pHead);

  if ( pHead->pStats == NULL )
    {
      return ELIFO_UNINIT;
    }
  else
    {
      pHead->options.stats= stats;

      return ELIFO_OK;
    }
#else
  UNUSED_PARAM(pHead);
  UNUSED_PARAM(stats);
  return ELIFO_UNAVAIL;
#endif
}



/*=====================================================================================\
| msg_next: allows to get the next message in the queue after the poper has gotten all |
|           the messages through msg_pop.                                              |
\=====================================================================================*/

void *     
msg_next( void *pUserNode )
{
  if ( pUserNode == NULL || PLIFONODE_FROM_PUSERNODE(pUserNode)->pNext == NULL )
    {
      return NULL;
    }
  else
    {
      return (void *) ( PLIFONODE_FROM_PUSERNODE(pUserNode)->pNext + 1 ); 
    }
}

/*=====================================================================================\
| msg_reverse: when using msg pattern, we might need to get the nodes in the FIFO and  |
|              not in the LIFO order. This function will 'reverse' the nodes so that   |
|              the worker can read them in the order they were pushed.                 |
|              The function returns the last node (last in LIFO order, thus first in   |
|              FIFO order), or return NULL if a NULL pUserNode is passed.              |
\=====================================================================================*/

void *     
msg_reverse( void *pUserNode )
{
  struct lifo_node *pCur,*pNext,*pTmp;

  if ( pUserNode == NULL )
    {
      return NULL;
    }

  pCur=PLIFONODE_FROM_PUSERNODE(pUserNode);
  for ( pNext= pCur->pNext, pCur->pNext= NULL;
        pNext != NULL;
        pCur= pNext, pNext= pTmp
      )
    {
      pTmp= pNext->pNext;
      pNext->pNext= pCur;
    }
  return pCur + 1;
}

/*=====================================================================================\
| PUSH & PULL                                                                          |
| -----------                                                                          |
|  Here starts the real push & pull algorithms!                                        |
|                                                                                      |
\=====================================================================================*/



#if STATS
  #define LIFO_START_STATS( pHead )                                              \
                      bool fStats =  ( pHead->options.stats &&                   \
                                       pHead->pStats != NULL                     \
                                     )
  #define LIFO_PASS_FSTATS  , fStats
  #define LIFO_INC_CNT( pHead, stat_index )                                      \
                      if ( fStats )                                              \
                        {                                                        \
                          __sync_fetch_and_add( pHead->pStats + stat_index, 1 ); \
                        }

  #define LIFO_MAX_CNT( pHead, value, stat_index )                               \
                      if ( fStats )                                              \
                        {                                                        \
                          register unsigned long max;                            \
                          do                                                     \
                            {                                                    \
                              max= pHead->pStats[stat_index];                    \
                            }                                                    \
                          while (   value > max &&                               \
                                  ! __sync_bool_compare_and_swap                 \
                                          ( pHead->pStats + stat_index,          \
                                            max,                                 \
                                            value                                \
                                          )                                      \
                                );                                               \
                        }

#else
  #define LIFO_START_STATS( pHead )
  #define LIFO_PASS_FSTATS
  #define LIFO_INC_CNT( pHead, stat_counter )
  #define LIFO_MAX_CNT( pHead, value, stat_index )
#endif


static int
common_push( struct lifo_head *pHead,
             void             *pUserNode)
{
  full_check(pHead);
  ptr_check_null(pUserNode);

  LIFO_START_STATS( pHead );
  LIFO_INC_CNT(  pHead, LIFO_STAT_PUSH_TOTAL );


  return pHead->push( pHead,
                      ((struct lifo_node *)( ((char *)pUserNode) )) - 1
                      LIFO_PASS_FSTATS
                    );
}

int
lifo_push( struct lifo_head *pHead,
           void             *pUserNode )
{
  ptr_check_null(pHead->push);
  return common_push( pHead, pUserNode );
}

int
msg_push( struct msg_head *pHead,
          void            *pUserNode )
{
  ptr_check_null(pHead->push);
  return common_push( pHead, pUserNode );
}


static int
common_pop ( struct lifo_head  *pHead,
             void             **ppUserNode,
             int              (*pop)(struct lifo_head *, void **  FSTATS)
           )
{
  int error;

  full_check(pHead);
  ptr_check_null(ppUserNode);

  LIFO_START_STATS( pHead );
  LIFO_INC_CNT(  pHead, LIFO_STAT_POP_TOTAL );


  if ( pHead->pFirst == NULL )
    {
      *ppUserNode= NULL;
      LIFO_INC_CNT( pHead, LIFO_STAT_POP_FAST_NULL );
      return ELIFO_OK;
    }

  error= pop(pHead, ppUserNode  LIFO_PASS_FSTATS);
  if ( *ppUserNode != NULL )
    {
      *ppUserNode= (error == 0) ? ((char *)*ppUserNode) + sizeof(struct lifo_node)
                                : NULL;
    }
  else
    {
      if (error == 0)
        {
          LIFO_INC_CNT( pHead, LIFO_STAT_POP_SLOW_NULL );
        }
    }
  return error;    
}

int
lifo_pop ( struct lifo_head  *pHead,
           void             **ppUserNode )
{
  ptr_check_null(pHead->lpop);
  return common_pop( pHead, ppUserNode, pHead->lpop );
}

int
msg_pop ( struct msg_head  *pHead,
          void            **ppUserNode )
{
  ptr_check_null(pHead->mpop);
  return common_pop( pHead, ppUserNode, pHead->mpop );
}


/*=====================================================================================\
| NONE algorithm.                                                                      |
| Notes: this algorithm is suitable ONLY on single threaded, it does NOT handle any    |
| ------ sort of lock to prevent contention.                                           |
|        For pop, we don't need to test pFirst to be NULL as it is done by the main,   |
|        thus if it is NULL we are not called. If it is not NULL we are called and it  |
|        can't be changed to NULL as we are not multi-threaded.                        |
|        We can also omit setting *pNode as that too was done by main pop!             |
\=====================================================================================*/

static int
lifo_push_N( struct lifo_head *pHead,
             struct lifo_node *pNode
             FSTATS_UNUSED )
{
  pNode->pNext = pHead->pFirst;
  pHead->pFirst= pNode;
  return ELIFO_OK;
}

static int
lifo_pop_N( struct lifo_head  *pHead,
            void             **ppNode
            FSTATS_UNUSED )
{
  *ppNode= pHead->pFirst;
  pHead->pFirst= pHead->pFirst->pNext;
  return ELIFO_OK;
}

static int
msg_pop_N( struct msg_head  *pHead,
           void            **ppNode
           FSTATS_UNUSED )
{
  *ppNode= pHead->pFirst;
  pHead->pFirst= NULL;
  return ELIFO_OK;
}



/*=====================================================================================\
| LOCK (mutexes) algorithm.                                                            |
| Notes: this is very straightforward and simple. Only thing to comment is that we     |
| ------ HAVE TO check again for NULL pointer on pop, even though we already did that  |
|        on the general lifo_pop function, because nodes might have been poped since   |
|        we did the test in the general pop, thus we might have NULL here although it  |
|        was not NULL in lifo_pop. This comment goes for all 3 algorithms. The test    |
|        in lifo_pop is just a shortcut to avoid calling the specific functions if we  |
|        find no node to pop at the instant we are there.                              |
|        The Mutexes are locked and unlocked by macros. Doing so, when stats are on    |
|        we can add that in the lock and unlock. The counter is incremented after      |
|        the lock is released, to ease contentions.                                    |
\=====================================================================================*/

#if STATS
#define MUTEX_CHECK_AND_LOCK( pMutex )             \
        register bool busy= false;                 \
        error= pthread_mutex_trylock( pMutex );    \
        if (error == EBUSY)                        \
          {                                        \
            busy= true;                            \
            error= pthread_mutex_lock( pMutex );   \
          }                                        \

#define MUTEX_STAT_AND_UNLOCK( pMutex, statIndex ) \
        error= pthread_mutex_unlock( pMutex );     \
        if (busy)                                  \
          {                                        \
            LIFO_INC_CNT( pHead, statIndex );      \
          }                                        \
        return error
#else
#define MUTEX_CHECK_AND_LOCK( pMutex )             \
        error= pthread_mutex_lock( pMutex )

#define MUTEX_STAT_AND_UNLOCK( pMutex, statIndex ) \
        return pthread_mutex_unlock( pMutex )
#endif

static int
lifo_push_L( struct lifo_head *pHead,
             struct lifo_node *pNode
             FSTATS )
{
  register int error;

  MUTEX_CHECK_AND_LOCK( &(pHead->mutex) );
  if (error)
    {
      return error;
    }
  pNode->pNext= pHead->pFirst;
  pHead->pFirst= pNode;
  MUTEX_STAT_AND_UNLOCK( &(pHead->mutex), LIFO_STAT_PUSH_CONTENDED );
}

static int
lifo_pop_L( struct lifo_head  *pHead,
            void             **ppNode
            FSTATS )
{
  register int error;

  MUTEX_CHECK_AND_LOCK( &(pHead->mutex) );
  if (error)
    {
      return error;
    }
  if ( (*ppNode= pHead->pFirst) != NULL )
    {
      pHead->pFirst= pHead->pFirst->pNext;
    }
  MUTEX_STAT_AND_UNLOCK( &(pHead->mutex), LIFO_STAT_POP_CONTENDED );
}


static int
msg_pop_L( struct msg_head  *pHead,
           void            **ppNode
           FSTATS )
{
  register int error;

  MUTEX_CHECK_AND_LOCK( &(pHead->mutex) );
  if (error)
    {
      return error;
    }
  *ppNode= (struct msg_node *)(pHead->pFirst);
  pHead->pFirst= NULL;
  MUTEX_STAT_AND_UNLOCK( &(pHead->mutex), LIFO_STAT_POP_CONTENDED );
}


#if CAS_ENABLED

/*=====================================================================================\
| CAS algorithm.                                                                       |
| Notes: this is the trickiest of the 3 algorithms as we can't use a pointer + age     |
| ------ counter to protect from ABA. So we need several CAS and protections.          |
|        As explained in the main header comment the global principle is as follow:    |
|        - we push with a simple CAS.                                                  |
|        - we want to pop the same... but there is the ABA problem.                    |
|        - To protect from that, pop is done in 2 phases.                              |
|        - pop phase 1, we CAS the pointer on the 1st node with a flag, taking         |
|          advantage that the pointers must be word aligned (at least) thus the last   |
|          bit is always free to use.                                                  |
|          So when the pointer to the 1st element is odd (flag to 1) it means some     |
|          thread started poping. If we enter either pop or push at that stage, we     |
|          try to help prior to continue with our own pop or push.                     |
|        - pop phase 2 just CAS the previously flags head pointer to the next node of  |
|          the queue. At this stage, we successfully poped the node and can return it. |
|                                                                                      |
|        Now comes the difficult part: helping!                                        |
|        - as helping is a tedious and slow processor, at least we try to optimize it  |
|          a bit. We combine the pending pop operation with our own push or pop.       |
|          Thus, if help succeeds, we are done with our operation too (or with phase   |
|          1 of the operation, if it was another pop).                                 |
|        - Best is to comment inside the code, easier to follow that way!              |
|                                                                                      |
| Backoff: when enabled at compile time, and it has not been set to 0 at runtime, the  |
| -------  backoff fires when the number of atomic operations is superior to the       |
|        threshold. It means that it will never fire on the first try (retries=1),     |
|        because if you want that, you can get it with the LOCK mode!                  |
|        Note that even when we trigger the backoff, we must continue the same algo-   |
|        rithm because some threads might have started pushing or popping before we    |
|        set the lock.                                                                 |
\=====================================================================================*/

#if STATS
  #define STAT_VARS  unsigned long loops=0, help=0 
  #define LOOP       loops++
  #define LIFO_COMMON_STATS( pHead, loops, PREFIX )                               \
          if ( fStats )                                                           \
            {                                                                     \
              __sync_fetch_and_add( pHead->pStats + PREFIX ## _TOTAL_ATOMICS,     \
                                    atomics                                       \
                                  );                                              \
              LIFO_MAX_CNT( pHead, atomics, PREFIX ## _MAX_ATOMICS );             \
              if ( loops > 1 )                                                    \
                {                                                                 \
                  LIFO_INC_CNT( pHead, PREFIX ## _CONTENDED );                    \
                }                                                                 \
            }

  #define LIFO_CAS_STATS( pHead, help, PREFIX )                                   \
          if ( fStats )                                                           \
            {                                                                     \
              LIFO_COMMON_STATS( pHead, loops, PREFIX );                          \
              if ( help > 0 )                                                     \
                {                                                                 \
                  __sync_fetch_and_add( pHead->pStats + PREFIX ## _HELPS,         \
                                        help                                      \
                                      );                                          \
                }                                                                 \
            }
#else
  #define STAT_VARS 
  #define LOOP
  #define LIFO_COMMON_STATS( pHead, loops, offset )
  #define LIFO_CAS_STATS( pHead, help, offset )
#endif

#if BACKOFF
  #define BACKOFF_VARS  bool locked= false;                             \
                        unsigned long backoff= 0;                       \
                        unsigned short int threshold=                   \
                                              pHead->options.threshold; \
                        int error
  #define BACKOFF_LOCK( pHead )                                         \
          if ( threshold != 0 )                                         \
            {                                                           \
              if ( backoff== 0 && atomics > threshold )                 \
                {                                                       \
                  backoff= __sync_add_and_fetch( &(pHead->backoff), 1 );\
                }                                                       \
              if ( locked== false && pHead->backoff != 0 )              \
                {                                                       \
                  error= pthread_mutex_lock( &(pHead->mutex) );         \
                  if (error)                                            \
                    {                                                   \
                      goto exiterror;                                   \
                    }                                                   \
                  locked= true;                                         \
                }                                                       \
            }

  #define BACKOFF_UNLOCK( pHead, PREFIX )                               \
          if ( locked == true )                                         \
            {                                                           \
              error= pthread_mutex_unlock( &(pHead->mutex) );           \
              LIFO_INC_CNT( pHead, PREFIX ## _BACKOFFED );              \
            }                                                           \
          else                                                          \
            {                                                           \
              error= ELIFO_OK;                                          \
            }                                                           \
       exiterror:                                                       \
          if ( backoff != 0 )                                           \
            {                                                           \
              __sync_fetch_and_sub( &(pHead->backoff), 1 );             \
              if ( backoff == 1 )                                       \
                {                                                       \
                  LIFO_INC_CNT( pHead  , PREFIX ## _1st_BACKOFFS );     \
                }                                                       \
              else                                                      \
                {                                                       \
                  LIFO_INC_CNT( pHead  , PREFIX ## _SUP_BACKOFFS );     \
                }                                                       \
            }

  #define CAS_RETURN   return error
#else
  #define BACKOFF_VARS
  #define BACKOFF_LOCK( pHead )
  #define BACKOFF_UNLOCK( pHead, PREFIX )
  #define CAS_RETURN   return ELIFO_OK
#endif

#if STATS || BACKOFF
  #define BOTH_VARS     unsigned long atomics= 0
  #define ATOMIC(i)     atomics += i
#else
  #define BOTH_VARS
  #define ATOMIC(i)
#endif

#define CAS_VAR        STAT_VARS; BACKOFF_VARS; BOTH_VARS
#define DCAS_VAR                  BACKOFF_VARS; BOTH_VARS




#define LIFO_CAS_LOCK      1
#define LIFO_CAS_COUNT_INC 2
#define LIFO_CAS_PUSH_RDY  1


static int
lifo_push_C( struct lifo_head *pHead,
             struct lifo_node *pNode
             FSTATS )
{
  uintptr_t         head_Lock;
  struct lifo_node *pFirst;
  bool              swapOK;
  struct lifo_node *pChained;
  CAS_VAR;
#if STATS
  unsigned long chain  = 1;
#endif

  node_check_aligned(pNode, sizeof(struct lifo_node));

restart:
                                       /* This is ABA protection. If the count is     */
  if ( pNode->count != 0 )             /* not 0, it means we have a helper in its     */
    {                                  /* critical section. Thus we don't push this   */
                                       /* node back, as it could then create an ABA   */
      if (                             /* situation, but mark the node as "pushable". */
                                       /* This is done using the last bit of the      */
                                       /* counter which is free as we count on pur-   */
                                       /* pose by increments of 2. So, to 'flag' the  */
                                       /* node, we add 1 to the count.                */

           __sync_add_and_fetch(
                                 &(pNode->count),
                                 LIFO_CAS_PUSH_RDY
                               )
                              == LIFO_CAS_PUSH_RDY
         )
                                       /* If count is equal to the flag after add, it */
        {                              /* means that helping threads 'sub-ed' the     */
                                       /* count while we where adding and we have no  */
                                       /* more helpers in their critical section. We  */
                                       /* must now put the count back to 0 with a CAS */
                                       /* as we might still compete with helpers that */
                                       /* just 'sub-ed' and would try to push the     */
                                       /* current node back. CAS ensures that only    */
                                       /* one thread: this one or a helper will       */
                                       /* succeed and push back the node.             */
          if ( !__sync_bool_compare_and_swap(
                                              &(pNode->count),
                                              LIFO_CAS_PUSH_RDY,
                                              0
                                            )
             )                         /* And so, if we don't succeed with the CAS    */
            {                          /* We are done as we know another helper will  */
                                       /* push back this node.                        */
              LIFO_INC_CNT( pHead, LIFO_STAT_PUSH_DELAYED );
              ATOMIC(2);
              goto exitok;
            }
        }
      else                             /* And same here, the count was not + ready-to */
        {                              /* push flag, thus we return too letting a     */
                                       /* helper push this node.                      */
          LIFO_INC_CNT( pHead, LIFO_STAT_PUSH_DELAYED );
          ATOMIC(1);
          goto exitok;
        }
      ATOMIC(1);
    }

  /*----------------------------------------------------------------------------------*/
  /* NOTE: from this point, we have seen 0 helpers in their critical section. It does */
  /* not mean there are 0 helpers, as they might still be some that didn't yet reach  */
  /* the critical section, but even so, it is safe to push the current node back now. */
  /* A little bit like RCU, we have seen all the "dangerous" helpers (those in        */
  /* critical section) hit their grace period, subsequent helpers will either fail    */
  /* because the pFirst in the head of the queue is now different, or if it becomes   */
  /* the same again (this node pushed, then poped-phase 1) it will succeed, but then  */
  /* the helper will help the next iteration of push/pop without ABA problem.         */
  /*----------------------------------------------------------------------------------*/

  pChained= pNode;

  do
    {
      LOOP;
      while(1)
        {
          BACKOFF_LOCK( pHead );
                                       /* This is the helping loop.                   */
                                       /* We start reading the pointer to first node  */
                                       /* (in head_Lock) then un-flag it to have a    */
                                       /* 'clean' pointer we can use in pFirst        */
          head_Lock= (uintptr_t)pHead->pFirst;
          pFirst   = (struct lifo_node *)(head_Lock & ~LIFO_CAS_LOCK);

                                       /* If either pFirst is NULL or the flag is not */
                                       /* present, we don't need to help and can      */
                                       /* proceed to our normal push operation.       */
          if ( pFirst == NULL || !(head_Lock & LIFO_CAS_LOCK) )
            {
              break;                   /* From here, we detected we need to help.     */
            }                          /* We initialize the successful-help flag and  */
                                       /* need a compile-only barrier to avoid the    */
                                       /* compiler to push this initialisation        */
                                       /* further down. We want to do the maximum     */
                                       /* things before really helping, as it is      */
#if STATS                              /* always faster if we don't need to help at   */
          help++;                      /* all. Thus it is important that this init-   */
#endif                                 /* ialisation stays here.                      */
          swapOK=false;
                                       /* Here we enter the (in)famous 'critical      */
                                       /* section'.                                   */
                                       /* So first we add to the count of help for    */
                                       /* the ABA protection (see comments above)     */
          ATOMIC(2);
          __sync_fetch_and_add( &(pFirst->count), LIFO_CAS_COUNT_INC );

                                       /* Then we MUST test wether it is still valid  */
                                       /* to help. The previous test alike was just   */
                                       /* an optimisation to avoid helping when       */
                                       /* possible, but this test is absolutely       */
                                       /* required for the algorithm to work.         */
          if ( head_Lock == (uintptr_t)pHead->pFirst )
            {
                                       /* As briefly explained above, intead of just  */
                                       /* helping the pop, we try to push our chain   */
                                       /* of nodes at the same time. So, should it    */
                                       /* succeed, we would be done both with the     */
                                       /* helping and with our push.                  */
              pChained->pNext= pFirst->pNext;
              ATOMIC(1);
              swapOK= __sync_bool_compare_and_swap( &(pHead->pFirst),
                                                      head_Lock,
                                                      pNode
                                                  );
            }
                                       /* But before anything, we must mark the end   */
          if (                         /* of critical section as soon as possible.    */
               __sync_sub_and_fetch( &(pFirst->count),
                                       LIFO_CAS_COUNT_INC
                                   )
                                ==     LIFO_CAS_PUSH_RDY
             )
            {
              ATOMIC(1);
              if (                     /* And same as above, if by applying the sub   */
                                       /* we came down to the value of the push-ready */
                                       /* flag, we CAS it to 0, to see if we get the  */
                                       /* 'token' (success)                           */
                   __sync_bool_compare_and_swap( &(pFirst->count),
                                                   LIFO_CAS_PUSH_RDY,
                                                   0
                                               )
                 )
                {
                                       /* And so here we succeeded, thus we have now  */
                                       /* the responsability to push back the node we */
                                       /* were helping.                               */

                  if (swapOK)          /* If we already succeeded helping, our push   */
                    {                  /* is done, and thus the last operation is     */
                      pNode= pFirst;   /* pushing the node we were helping. We could  */
#if STATS                              /* recurse... but as it is terminal-recursion  */
                      loops--;         /* (because we have nothing else to do after   */
                      chain= 1;        /* we will finish pushing the helped node) we  */
#endif                                 /* directly jump to the start after replacing  */
                      LIFO_INC_CNT( pHead, LIFO_STAT_PUSH_EXCHANGED );
                      goto restart;    /* the parameter node by the new node to push  */
                    }                  /* (head is the same). The compiler might have */
                                       /* detected this terminal recursion but better */
                                       /* safe than sorry!                            */

                  else                 /* If we didn't succeed the helping CAS, it    */
                    {                  /* means that our node (the one we were called */
                                       /* with in the second parameter) is still to   */
                                       /* be pushed, and now we have a second node to */
                                       /* push to. So here we do another optimisation */
                                       /* instead of pushing only one node (the one   */
                                       /* in the params), we consider we push a chain */
                                       /* of nodes. Initially this chain contains     */
                                       /* only one node, the one we were called with, */
                                       /* see the initialisation at the start of the  */
                                       /* function. So now, what we just need to do   */
                                       /* is just add the helped node to the chain of */
                                       /* nodes to push, and loop back.               */
                      pChained->pNext= pFirst;
                      pChained= pFirst;
#if STATS
                      chain++;
                      LIFO_INC_CNT( pHead, LIFO_STAT_PUSH_CHAINED );
                      LIFO_MAX_CNT( pHead, chain, LIFO_STAT_PUSH_LONGEST_CHAIN );
#endif
                      continue;
                    }
                }
            }
                                       /* Here we have no responsability to push back */
                                       /* the helped node, so if we succeeded the     */
          if (swapOK)                  /* help-CAS, we are do, due to the optimi-     */
            {                          /* sation already commented above.             */
              LIFO_INC_CNT( pHead, LIFO_STAT_PUSH_OPTIMIZED );
              goto exitok;
            }
        }

                                       /* And here, we are done helping (or didn't    */
                                       /* need to) thus we simply try to CAS our node */
                                       /* in the LIFO queue. We use pChained as       */
                                       /* explained above (optimisation in case we    */
                                       /* inherit the responsability to push back     */
                                       /* some helped nodes).                         */
      pChained->pNext= pFirst;
      ATOMIC(1);
    }   
   while ( ! __sync_bool_compare_and_swap ( &(pHead->pFirst), pFirst, pNode ) );

                                       /* And as usual with this sort of CAS, if it   */
                                       /* succeeds we are done pushing, otherwise we  */
exitok:                                /* will loop to the start: helping, pushing... */
  BACKOFF_UNLOCK( pHead, LIFO_STAT_PUSH );
  LIFO_CAS_STATS( pHead, help, LIFO_STAT_PUSH );
  CAS_RETURN;
}



static int
lifo_pop_C( struct lifo_head  *pHead,
            void            **ppNode
            FSTATS )
{
  uintptr_t         head_Lock;
  struct lifo_node *pFirst;
  CAS_VAR;

  do
    {
      LOOP;
      while(1)
        {                              /* We start reading the pointer to first node  */
                                       /* (in head_Lock). If it is 0, there are no    */
                                       /* nodes to pop, thus the returned value for   */
                                       /* the poped node is NULL, and we are done.    */
          head_Lock= (uintptr_t)pHead->pFirst;
          if (head_Lock == 0)
            {
              pFirst= NULL;
              goto exitok;
            }
                                       /* Then we 'clean' the head_lock of the poten- */
                                       /* tial flag, so that we get a valid pointer.  */
          pFirst  = (struct lifo_node *)(head_Lock & ~LIFO_CAS_LOCK);

          BACKOFF_LOCK( pHead );

                                       /* If the head_lock is NOT flagged, no help    */
                                       /* is needed, we can just jump to the real     */
                                       /* poping operations of pFisrt.                */
          if ( !(head_Lock & LIFO_CAS_LOCK) )
            {
              break;
            }                          /* And so here, we do really need to help!     */

  /*----------------------------------------------------------------------------------*/
  /* NOTE: the technique when helping could have been similar to that used on push:   */
  /* when helping pop, tries to pop also the current node. But as there are cases     */
  /* where we must push-back the node we helped, and as pop is done in 2 step, doing  */
  /* so would have resulted in a very complicated, and questionably faster algorithm. */
  /* For example you must delay the push-back after the second phase otherwise the    */
  /* push-back will, in 100% of situations, have to help. You would also have to      */
  /* handle the chained push (as we might have several nodes to push-back), and       */
  /* because we are not in the push function, this would be slow and clumsy.          */
  /* Instead, there is an obvious optimisation: when we have to push-back, instead    */
  /* of doing so, then poping a node, we just have to return the node. This avoids    */
  /* both pushing-back and poping. It does also avoid having to recurse on push_C     */
  /* and as we return immediately, we don't care anymore of chained pushes!           */
  /* Overall the code is much more simple and readable, and less loops and calls.     */
  /* And because we want to have more chance to be the one who would push-back, we    */
  /* care less of retesting at the last moment if we do really have to pop, and thus  */
  /* this last minute test is removed too.                                            */
  /*----------------------------------------------------------------------------------*/


                                       /* For the comments, this section of help is   */
                                       /* exactly the same as in push: mark critical  */
#if STATS                              /* section, try to help, unmark, then try to   */
          help++;                      /* get the 'token' to push-back if we were     */
#endif                                 /* the last helper.                            */

          ATOMIC(2);
          __sync_fetch_and_add( &(pFirst->count), LIFO_CAS_COUNT_INC );

          if ( head_Lock == (uintptr_t)pHead->pFirst )
            {
              ATOMIC(1);
              __sync_bool_compare_and_swap(
                                            &(pHead->pFirst),
                                              head_Lock,
                                              pFirst->pNext
                                          );
            }

          if (                         /* Unmark of critical section.                 */
               __sync_sub_and_fetch( &(pFirst->count),
                                       LIFO_CAS_COUNT_INC
                                   )
                                ==     LIFO_CAS_PUSH_RDY
             )
            {
              ATOMIC(1);
              if (                     /* Try to get 'token' as we were last helper   */
                   __sync_bool_compare_and_swap( &(pFirst->count),
                                                   LIFO_CAS_PUSH_RDY,
                                                   0
                                               )
                 )
                {                      /* So here we have this node to push-back as   */
                                       /* we were last helper and got the 'token'.    */
                                       /* We didn't yet start poping, thus, the super */
                                       /* optimisation, logical with LIFO, is to      */
                  LIFO_INC_CNT( pHead, LIFO_STAT_POP_OPTIMIZED );
                  goto exitok;         /* avoid doing a push followed by a pop        */
                                       /* (which would, if not interrupted by others  */
                                       /* return that node), we just return the node  */
                }                      /* we just helped.                             */
            }
        }

                                       /* Here we are done helping (or didn't need    */
                                       /* to help) and we do the 'real' pop in 2      */
                                       /* phases. This 1st phase tries to 'flag' the  */
                                       /* pointer to the first node.                  */
                                       /* If it fails, it means we had another push   */
                                       /* or pop at the same time, thus we loop.      */
      ATOMIC(1);
    }
  while ( ! __sync_bool_compare_and_swap ( &(pHead->pFirst),
                                             pFirst,
                                             head_Lock | LIFO_CAS_LOCK
                                         )
        );

                                       /* If it succeeds, we do the phase 2 that      */
                                       /* consists in CAS-ing the address of the      */
                                       /* second node (pFirst->pNext). Due to the     */
                                       /* helping process we don't need to loop here. */
                                       /* We don't even need to test the success!...  */
                                       /* If it succeeds, all is well, if it fails,   */
                                       /* it means that one of the helpers succeeded  */
                                       /* thus all is well too!                       */
  ATOMIC(1);
  __sync_bool_compare_and_swap( &(pHead->pFirst),
                                  head_Lock | LIFO_CAS_LOCK,
                                  pFirst->pNext
                              );
exitok:
  BACKOFF_UNLOCK( pHead, LIFO_STAT_POP );
  LIFO_CAS_STATS( pHead, help, LIFO_STAT_POP );
                                       /* And so now, the node has been poped succes- */
                                       /* sfully, we can store it and return.         */
  *ppNode= pFirst;
  CAS_RETURN;
}

/*=====================================================================================\
| NOTE: with the "message passing" algorithm, we don't have to handle ABA. ABA appears |
|       only during pop, and msg has no issue because it takes ALL the nodes. We just  |
|       need to protect pushers from themselves and popers. The popers pop ALL the     |
|       queue, and not 1 node at a time. This makes the algorithm wait-free for the    |
|       poper as it only needs to do an exchg replacing the pointer to the first node  |
|       by NULL while reading this 1st node. Then of course, the poper MUST handle ALL |
|       the nodes that were poped, as they might be several.                           |
|       This algorithm also has other advantages: when poped, the nodes can be         |
|       reclaimed as push do never read/write data inside the nodes themselves, nor    |
|       keep any pointer to do so (except of course for the node we push, but that is  |
|       safe). Thus, once poped, the nodes can be safely reclaimed.                    |
|       Although the pop part is now wait-free, the push part is lock free but can     |
|       still suffer from live-lock, unless backoffed.                                 |
|                                                                                      |
| IMPORTANT: there is absolutely no need for a DCAS algorithm for this "message        |
|       passing", as CAS is much enough to work in a simple and straightforward way.   |
|                                                                                      |
| NOTE ON BACKOFF: by itself, this pop being wait-free do not need backoff. But it can |
|       still interrupt a pusher. Anyway, as the popers all put NULL in pFirst, the    |
|       number of interrupts is limited.  If B is the backoff threshold, and T the     |
|       number of threads the theoretical maximum number of retries (for a pusher, as  |
|       poper have 0 count retry because of wait-free) is B + 2 *(T - 1) (note that if |
|       you have more threads than CPUs [C], the maximum becomes: B + 2 *(C - 1) )     |
|       This is higher than the possible value for lifo because a pusher having set    |
|       backoff flag and locked can still be interrupted by all the other (T - 1)      |
|       threads pushing, and each time, once more by a poper. This is probably a       |
|       theoretical maximum that won't be reached in "real life" unless you craft it   |
|       on purpose! Once all the pushers have gone on lock, the number of popers       |
|       running is irrelevant because as the pFirst is now NULL, they won't change     |
|       that thus won't interrupt a CAS anymore.                                       |
|       So DECISION was to NOT implement backoff here. The drawback of dropping a      |
|       wait-free algorithm and going into lock is too huge compared to the little     |
|       impact on pushers. If you feel the number of retried pushes could be too high  |
|       you can still lower the threshold variable. And if it still feels too much     |
|       you can still fallback to LOCK mode.                                           |
\=====================================================================================*/

static int
msg_pop_C( struct msg_head  *pHead,
           void           **ppNode
           FSTATS_UNUSED )
{

  *ppNode= (struct msg_node *)(__sync_fetch_and_and( &(pHead->pFirst), 0 ));

  return ELIFO_OK;
}



/*=====================================================================================\
| DCAS algorithm.                                                                      |
| Notes: the classic method to protect from ABA is using a counter. As we can use      |
| ------ DCAS, we can have both a word pointer and a full word counter, which is a     |
|        fair enough protection (especially in 64bits!).                               |
|        To efficiently protect from ABA we don't need to do a DCAS both in push and   |
|        in pop, one only is enough. So we do that in pop, and push does a single CAS. |
|        An important thing to notice, for ABA protection, is that we need to read     |
|        the age counter BEFORE the pointer. Obviously, if read after, it would not    |
|        protect us at all because someone could ABA us between the pointer and the    |
|        counter read!                                                                 |
|        And then, comes another complexity with the compiler optimizations. As there  |
|        is a condition on which we break the loop, the compiler will try to optimize  |
|        and WILL push down the reading of the age: it appears to be unrelated with    |
|        the test condition, so pushing it down, saves CPU when we break. To avoid     |
|        that, we MUST instruct the compiler NOT to do so, hence the compile-only      |
|        barrier before the test. Note that it is a "compile-only" barrier, it does    |
|        not spit out any sort of assembly code, and does not cost more CPU running    |
|        the program, it just instructs the compiler to NOT move the instructions      |
|        through the barrier (eg. pushing down instructions that were before, or       |
|        vice-versa)                                                                   |
\=====================================================================================*/

static int
lifo_push_D( struct lifo_head *pHead,
             struct lifo_node *pNode
             FSTATS )
{
  DCAS_VAR;

  node_check_aligned(pNode, sizeof(void *));

  do
    {
      ATOMIC(1);
      BACKOFF_LOCK( pHead );

      pNode->pNext= pHead->pFirst;
    }   
  while ( ! __sync_bool_compare_and_swap ( &(pHead->pFirst), pNode->pNext, pNode ) );

  BACKOFF_UNLOCK( pHead, LIFO_STAT_PUSH );
  LIFO_COMMON_STATS( pHead, atomics, LIFO_STAT_PUSH );
  CAS_RETURN;
}

static int
lifo_pop_D( struct lifo_head  *pHead,
            void            **ppNode
            FSTATS )
{
  DCAS_VAR;
  struct lifo_head old_Head, new_Head;

  do
    {
      ATOMIC(1);
      BACKOFF_LOCK( pHead );

      old_Head.age  = pHead->age;

      asm volatile ("" : : : "memory");    /* Compile only barrier: age MUST be read  */
                                           /* BEFORE pFirst to protect from ABA       */

      if ( (old_Head.pFirst= pHead->pFirst) == NULL )
        {
          break;
        }
      new_Head.age   = old_Head.age + 1;
      new_Head.pFirst= old_Head.pFirst->pNext;
    }
#ifdef LIFO_32
  while ( 
          ! __sync_bool_compare_and_swap (
                                            (__int64_t *)pHead     ,
                                          *((__int64_t *)&old_Head),
                                          *((__int64_t *)&new_Head) 
                                         )
        );
#else
  while ( 
          ! __sync_bool_compare_and_swap (
                                            (__int128_t *)pHead     ,
                                          *((__int128_t *)&old_Head),
                                          *((__int128_t *)&new_Head) 
                                         )
        );
#endif

  *ppNode= old_Head.pFirst;

  BACKOFF_UNLOCK( pHead, LIFO_STAT_POP );
  LIFO_COMMON_STATS( pHead, atomics, LIFO_STAT_POP );
  CAS_RETURN;
}

#endif /*CAS_ENABLED*/

