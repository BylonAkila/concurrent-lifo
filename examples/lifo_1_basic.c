/*=====================================================================
 Copyright (C) 2012 Alain BENEDETTI alainb06@free.fr

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published byr
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>
=====================================================================*/

/*=====================================================================================\
| Function:                                                                            |
|==========                                                                            |
| This is a basic example on how to use the LIFO library.                              |
| We are assuming the library was compiled by default without checks, thus we test     |
| return values only on setups (in case allocations of memory went wrong)              |
|--------------------------------------------------------------------------------------|
| Date: 2013-01-06                                                                     |
|======                                                                                |
|                                                                                      |
|--------------------------------------------------------------------------------------|
| Author: Alain BENEDETTI aka Zakhar                                                   |
|========                                                                              |
|                                                                                      |
\=====================================================================================*/

#include <stdio.h>
#include "lifo.h"


int
main()
{
  int                 error;
  struct lifo_head   *pHead;
  struct user_node
   {
     int   number  ;
     long  whatever;
   }                 *pUserNode;
  struct user_node   *pPoped;


/* STEP 1: setup the Head ============================================================*/
  error= lifo_setup_head( &pHead, LIFO_DCAS );
  if (error != 0)
    {
      printf("Error %d on lifo_setup_head\n", error);
      return error;
    }
  else
    {
      printf("OK: lifo_setup_head\n");
    }


/* STEP 2: setup the nodes you will use ==============================================*/
  error= lifo_setup_node( (void **)&pUserNode, sizeof(struct user_node) );
  if (error != 0)
    {
      printf("Error %d on lifo_setup_node\n", error);
      return error;
    }
  else
    {
      printf("OK: lifo_setup_node\n");
    }

  /* You would normally put some value in your node.
     This is your own user data here.                */
  pUserNode->number  = 1;
  pUserNode->whatever= 0;


/* STEP 3: push the node on the queue ================================================*/
  lifo_push( pHead, pUserNode );

/* STEP 4: pop a node from the queue  ================================================*/
  lifo_pop ( pHead, (void **)&pPoped );

  /* Now, here normally you got your own data back! */
  if (pPoped== NULL)
    {
      printf("No node was poped!\n");
    }
  else
    {
      printf("We poped the node number %d\n", pPoped->number);
    }


/* STEP 5: cleanup your nodes & queue ================================================*/
  lifo_cleanup_node(pUserNode);
  lifo_cleanup_head(pHead, false);

  printf("OK: all!\n");
  return 0;
}
