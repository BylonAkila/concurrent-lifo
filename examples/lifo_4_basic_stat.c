/*=====================================================================
 Copyright (C) 2012 Alain BENEDETTI alainb06@free.fr

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published byr
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>
=====================================================================*/

/*=====================================================================================\
| Function:                                                                            |
|==========                                                                            |
| This is a basic example on how to use the LIFO library with features and stats.      |
| We are assuming the library was compiled by default without checks, thus we test     |
| return values only on setups (in case allocations of memory went wrong)              |
| We comment out only those steps added from lifo_1_basic                              |
|--------------------------------------------------------------------------------------|
| Date: 2013-01-07                                                                     |
|======                                                                                |
|                                                                                      |
|--------------------------------------------------------------------------------------|
| Author: Alain BENEDETTI aka Zakhar                                                   |
|========                                                                              |
|                                                                                      |
\=====================================================================================*/

#include <stdio.h>
#include "lifo.h"

#define NUM_ITER 3


/*---- This is the meaning of each statistic, each correspond to a given #define -----*/

static char *stat_label[]=
  {  "Push - total        ",
     "Pop  - total        ",
     "Pop  - fast null    ",
     "Push - contended    ",
     "Pop  - contended    ",
     "Pop  - slow null    ",

     "Push - backoffed    ",
     "Push - 1st backoffs ",
     "Push - sup. backoffs",
     "Push - total atomics",
     "Push - max   atomics",

     "Pop  - backoffed    ",
     "Pop  - 1st backoffs ",
     "Pop  - sup. backoffs",
     "Pop  - total atomics",
     "Pop  - max   atomics",

     "Push - helps        ",
     "Pop  - helps        ",
     "Push - optimized    ",
     "Pop  - optimized    ",
     "Push - delayed      ",
     "Push - exchanged    ",
     "Push - chained      ",
     "Push - longest chain"
  };

/*---- Same to display the modes nicely, and not only their internal code ------------*/
static char *mode_label[]=
  {
    "ERROR",
    "NONE" ,
    "LOCK" ,
    "CAS"  ,
    "DCAS"
  };


int
main()
{
  int                 error, i;
  unsigned long       stat ;
  struct lifo_head   *pHead;
  struct user_node
   {
     int   number  ;
     long  whatever;
   }                 *pUserNode;
  struct user_node   *pPoped;

/* STEP 0: We display all the library features that depend on the module compilation  */
  printf("\nThese are the library features:\n");
  printf("- Backoff          : %s\n", (LIFO_IS_BACKOFF)     ? "enabled" : "disabled" );
  printf("- Checks           : %s\n", (LIFO_IS_CHECK)       ? "enabled" : "disabled" );
  printf("- CAS              : %s\n", (LIFO_IS_CAS_ENABLED) ? "enabled" : "disabled" );
  printf("- Target           : %s\n", (LIFO_IS_TARGET_INTEL)? "Intel"   : "not Intel");
  printf("- 64 bits          : %s\n", (LIFO_IS_64)          ? "yes"     : "no"       );
  printf("- cache alignment  : %u\n", lifo_cache_align );
  printf("- size of lifo_node: %u\n", lifo_sizeof_node );
  printf("- Default lock mode: %s\n", mode_label[lifo_defaults.mode] );
  printf("- Statistics       : %s\n", (lifo_defaults.stats) ? "enabled" : "disabled" );


  error= lifo_setup_head( &pHead, LIFO_DEF_MODE );
  if (error != 0)
    {
      printf("Error %d on lifo_setup_head\n", error);
      return error;
    }
  else
    {
      printf("OK: lifo_setup_head\n");
    }


/* STEP 1-Bis: now we add statistics =================================================*/
  error= lifo_setup_stats( pHead );
  if (error != 0)
    {
      if (error == ELIFO_UNAVAIL)
        {
          printf("Statistics are not available!\n");
        }
      else
        {
          printf("Error %d on lifo_setup_head\n", error);
          return error;
        }
    }
  else
    {
      printf("OK: lifo_setup_stats\n");
    }


  error= lifo_setup_node( (void **)&pUserNode, sizeof(struct user_node) );
  if (error != 0)
    {
      printf("Error %d on lifo_setup_node\n", error);
      return error;
    }
  else
    {
      printf("OK: lifo_setup_node\n");
    }

  pUserNode->number  = 1;
  pUserNode->whatever= 0;


/* STEP 2bis: we iterate a little bit on push/pop to run the stats ===================*/
  for (i=0 ; i<NUM_ITER; i++)
    {
      lifo_push( pHead, pUserNode );
      lifo_pop ( pHead, (void **)&pPoped );
      if (pPoped == NULL)
        {
          break;
        }
    }

  if (pPoped== NULL)
    {
      printf("No node was poped!\n");
    }
  else
    {
      printf("We poped the node number %d\n", pPoped->number);
    }


/* STEP 4bis: we print our statistics ================================================*/
  printf("Statistics:\n");
  for (i=0; lifo_stat( pHead, i, &stat ) == ELIFO_OK; i++)
    {
      printf("%s: %lu\n", stat_label[i], stat );
    }


  lifo_cleanup_node(pUserNode);

  lifo_cleanup_head(pHead, false);

/* Note that lifo_cleanup_head also cleans our stats.
   So no need to do that separately as we are just exiting */

  printf("OK: all!\n");
  return 0;
}
