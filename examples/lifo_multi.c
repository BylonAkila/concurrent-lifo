/*=====================================================================
 Copyright (C) 2012 Alain BENEDETTI alainb06@free.fr

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published byr
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>
=====================================================================*/

/*====================================================================================\
| Function:                                                                           |
|==========                                                                           |
| This is the test program for the "library" LIFO.                                    |
| It runs:                                                                            |
| - a simple test (mono-thread) pushing and poping some nodes in sequence             |
| - a multi-threaded test which can then be timed to measure.                         |
| - displays compiled features.                                                       |
|                                                                                     |
| There is a basic argument scanning:                                                 |
| -1) Number of threads (defaults to the number of detected CPUs)                     |
| -2) Number of iterations (defaults to 1,000,000)                                    |
| -3) Lock mode: L (Lock), C (CAS), D (DCAS) and 2 special: M (malloc/free), N (none) |
|     N (none) serves to measure the time taken by the loops themselves. If you       |
|     want to time how much it takes on a said mode, you should first to a timing     |
|     in this mode, then do a timing with same parameters on N (none) mode and        |
|     compute the difference which is the "real" time spent by locks and idling.      |
|     M (malloc/free), instead of pushing/poping nodes, each time they are malloc-ed/ |
|     freed, which serves to compare the time with locks and the times or not         |
|     queuing the nodes, but simply freeing them and allocating them again when needed|
| -4) When a 4th parameter (any value) is specified, the pointer checks will be       |
|     deactivated. Note that this can lead to core dumps (normally not with this test |
|     program) as CAS/DCAS will provide unpredictable results on unaligned buffers.   |
|-------------------------------------------------------------------------------------|
| Tested: Ubuntu Precise (12.04 x64 and i386)                                         |
|========                                                                             |
|                                                                                     |
|-------------------------------------------------------------------------------------|
| Version: 0.1.0                                                                      |
|=========                                                                            |
|                                                                                     |
|-------------------------------------------------------------------------------------|
| Date: 2012-05-22                                                                    |
|======                                                                               |
|                                                                                     |
|-------------------------------------------------------------------------------------|
| Author: Alain BENEDETTI aka Zakhar                                                  |
|========                                                                             |
|                                                                                     |
|-------------------------------------------------------------------------------------|
| History:                                                                            |
|=========                                                                            |
|                                                                                     |
|-------------------------------------------------------------------------------------|
| Compiling instructions:                                                             |
|========================                                                             |
|                                                                                     |
| $ gcc -Wall -D_GNU_SOURCE -O3 -o lifo_test.o -c lifo_test.c                         |
|                                                                                     |
| No other special option are used.                                                   |
\====================================================================================*/

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <stdlib.h>
#include <sched.h>
#include <linux/unistd.h>
#include <sys/syscall.h>
#include <errno.h>
#include <pthread.h>
#include "lifo.h"

#define LIFO_ALLOC 999

static char *stat_label[]=
  {  "Push - total        ",
     "Pop  - total        ",
     "Pop  - fast null    ",
     "Push - contended    ",
     "Pop  - contended    ",
     "Pop  - slow null    ",

     "Push - backoffed    ",
     "Push - 1st backoffs ",
     "Push - sup. backoffs",
     "Push - total atomics",
     "Push - max   atomics",

     "Pop  - backoffed    ",
     "Pop  - 1st backoffs ",
     "Pop  - sup. backoffs",
     "Pop  - total atomics",
     "Pop  - max   atomics",

     "Push - helps        ",
     "Pop  - helps        ",
     "Push - optimized    ",
     "Pop  - optimized    ",
     "Push - delayed      ",
     "Push - exchanged    ",
     "Push - chained      ",
     "Push - longest chain"
  };


static unsigned long INC_TO=1000000; /* one million... */

static unsigned long        junk;
static unsigned long       *global_count;
static unsigned long       *global_wait;
static          int         ncpu;
static unsigned int         wait=10;
static unsigned int         procs = 0;
static struct lifo_head    *pHead = NULL;

static void errorexit( int error, const char * pMsg, bool fExit )
{
  if (error)
    {
      printf("Error %d: %s\n", error, pMsg);
      if (errno != 0)
        {
          perror("System error");
        }
      if (fExit)
        {
          if (pHead != NULL)
            {
              lifo_cleanup_head(pHead, true);
            }
          exit(error);
        }
    }
}


static pid_t gettid( void )
{
  return syscall( __NR_gettid );
}


static char *
mode_str( lifo_mode mode )
{
  switch (mode)
    {
      case LIFO_LOCK : return "LOCK" ;
      case LIFO_CAS  : return "CAS"  ;
      case LIFO_DCAS : return "DCAS" ;
      case LIFO_NONE : return "NONE" ;
      default        : return "ERROR";
    }
}



static void *thread_routine( void *arg )
{
  unsigned long  i;
  unsigned long  j;
  unsigned long  count=0;
  unsigned int   proc_num = (int)(uintptr_t)arg;
  cpu_set_t      set;
  unsigned long *q;

  if (ncpu>1 && proc_num < (unsigned int)ncpu)
    {
      CPU_ZERO( &set );
      CPU_SET( proc_num, &set );

      if (sched_setaffinity( gettid(), sizeof( cpu_set_t ), &set ))
        {
          errorexit( errno, "sched_setaffinity", true );
        }
    }

  for (i = 0; i < INC_TO; i++)
    {
      do
        {
          errorexit( lifo_pop( pHead, (void **)&q ), "pop error", true );
          global_wait[proc_num]++;
        }
      while (q == NULL );

      for (j=0; j<=wait * (1 + ((proc_num + i) & 0xF)); j++)
        count += j;
      errorexit( lifo_push( pHead, q ), "push error", true );
      global_count[proc_num]++;
    }

  junk= count; /* To avoid compiler "optimize" count completely because unused. */
  return NULL;
}


static 	void usage(void)
{
  printf("\
lifo_multi usage:\n\
lifo_multi [nThr|-h|--help [nNodes [nIter [mode [bkOff [fStat [fPtrCk]]]]]]]\n\
nThr: number of threads.\n");
}

int
main( int   argc, char *argv[] )
{
  unsigned long  i;
  unsigned long  total_wait=0;
  unsigned long  total_count=0;
  unsigned long *pNode;
  unsigned int   nNodes= 0;
           int   error;
  unsigned long  stat;
  struct lifo_options selected_options;
  pthread_t     *pThrs;

  /* Set our options to default first ************************************************/
  struct lifo_options  options;
  options.threshold = lifo_defaults.threshold;
  options.stats     = lifo_defaults.stats    ;
  options.mode      = lifo_defaults.mode     ;

  /* Basic argument scanning *********************************************************/

  if (argc>1)
    {
      if ( strcmp(argv[1],"-h") == 0 || strcmp(argv[1],"--help") == 0 )
        {
          usage();
          return 0;
        }
      printf("Arg 1 found, scanning (number of threads): %s\n",argv[1]);
      sscanf(argv[1],"%u",&procs);
    }
  if (argc>2)
    {
      printf("Arg 2 found, scanning (number of nodes): %s\n",argv[2]);
      sscanf(argv[2],"%u",&nNodes);
    }
  if (argc>3)
    {
      printf("Arg 3 found, scanning (number of iterations): %s\n",argv[3]);
      sscanf(argv[3],"%lu",&INC_TO);
    }
  if (argc>4)
    {
      printf("Arg 4 found, scanning (lock mode): %s\n",argv[4]);
      switch(argv[4][0])
        {
          case 'c':
          case 'C': options.mode= LIFO_CAS;
                    break;
          case 'd':
          case 'D': options.mode= LIFO_DCAS;
                    break;
          case 'l':
          case 'L': options.mode= LIFO_LOCK;
                    break;
          case 'n':
          case 'N': options.mode= LIFO_NONE;
                    break;
          default : break;
        }
    }
  if (argc>5)
    {
      printf("Arg 5 found, scanning (wait multiplier): %s\n",argv[5]);
      sscanf(argv[5],"%u",&wait);
    }
  if (argc>6)
    {
      printf("Arg 6 found, scanning (backoff threshold): %s\n",argv[6]);
      sscanf(argv[6],"%hu",&options.threshold);
    }
  if (argc>7 && argv[7]!=NULL)  /* Stats deactivated if we have a 6th argument */
    {
      printf( "Arg 7 found (%s), deactivating stats. Stats were: %s\n",
              argv[7],
              (lifo_defaults.stats)? "enabled" : "disabled"
            );
      options.stats= false;
    }



  /* Display library features ********************************************************/
  printf("\nThese are the library features:\n");
  printf("- Backoff          : %s\n", (LIFO_IS_BACKOFF)     ? "enabled" : "disabled" );
  printf("- Checks           : %s\n", (LIFO_IS_CHECK)       ? "enabled" : "disabled" );
  printf("- CAS              : %s\n", (LIFO_IS_CAS_ENABLED) ? "enabled" : "disabled" );
  printf("- Target           : %s\n", (LIFO_IS_TARGET_INTEL)? "Intel"   : "not Intel");
  printf("- 64 bits          : %s\n", (LIFO_IS_64)          ? "yes"     : "no"       );
  printf("- cache alignment  : %u\n", lifo_cache_align );
  printf("- size of lifo_node: %u\n", lifo_sizeof_node );

  printf("- Default lock mode: %s\n", mode_str(lifo_defaults.mode) );
  printf("- Statistics       : %s\n", (lifo_defaults.stats) ? "enabled" : "disabled" );
  if (LIFO_IS_BACKOFF)
    {
      printf("- backoff threshold: %hu\n", lifo_defaults.threshold );
    }
  printf("\n\n");


  /* Do the multi-threaded test ******************************************************/

  /* Getting number of CPUs */
  ncpu= (int)sysconf( _SC_NPROCESSORS_ONLN );
  if (ncpu < 0)
    {
      errorexit( errno, "Sysconf error", true );
    }

  if (procs == 0)
    {
      procs = ncpu;
    }
  if (nNodes == 0)
    {
      nNodes = procs;
    }

  pThrs       = malloc( sizeof( pthread_t ) * procs );
  global_count= calloc( sizeof( *global_count ), procs );
  global_wait = calloc( sizeof( *global_wait ), procs );
  if (pThrs == NULL || global_count == NULL || global_wait == NULL)
    {
      errorexit( errno, "Allocation error", true );
    }

  /* Using the library ***************************************************************/
  error= lifo_setup_head( &pHead, options.mode );
  if (error != 0 || pHead == NULL)
    {
      errorexit( error, "lifo_setup_head", true );
    }

  printf(
          "\n\nMulti-threaded LIFO test with %d threads on %d cpu, head is %p\n",
          procs,
          ncpu,
          pHead
        );
  printf( "Lock model requested is: %s (%d)\n",
           mode_str( options.mode ),
           options.mode
        );
  errorexit( lifo_get_mode(pHead, &selected_options.mode), 
            "lifo_get_mode", true );
  printf( "Selected\nLock model is          : %s (%d)\n",
           mode_str( selected_options.mode ),
           selected_options.mode
        );

  if (LIFO_IS_BACKOFF)
    {
      errorexit( error= lifo_set_threshold(pHead, options.threshold),
                 "lifo_set_threshold", true );
      errorexit( error= lifo_get_threshold(pHead, &selected_options.threshold),
                 "lifo_get_threshold", true );
      printf( "Backoff threshold is   : %hu\n", selected_options.threshold );
    }
  else
    {
      printf( "Backoff threshold is   : disabled\n" );
    }

  if ( lifo_defaults.stats && options.stats )
    {
      errorexit( lifo_setup_stats( pHead ) , "lifo_setup_stats", true );
      errorexit( error= lifo_get_stats(pHead, &selected_options.stats),
                 "lifo_get_stats", false );
      printf( "Stats are              : %s\n", (selected_options.stats) ? "enabled" 
                                                                        : "disabled" );
    }
  else
    {
      printf( "Stats are              : disabled\n" );
    }

  lifo_set_stats(pHead, false);
  for (i = 0; i < nNodes; i++)
    {
      error= lifo_setup_node( (void **)&pNode, sizeof(*pNode) );
      if (error != 0 || pNode== NULL)
        {
          errorexit( error, "lifo_alloc_node", true );
        }
      *pNode= i;
      printf("Allocated node: %p, number:%ld\n",pNode,i);
      errorexit( lifo_push( pHead, pNode ), "lifo_push", true );
      printf("Pushed (number: %ld): OK\n", *pNode);
    }
  lifo_set_stats(pHead, options.stats);

  /* Doing the multithreaded push/pop ************************************************/
  for (i = 0; i < procs; i++)
    {
      if (pthread_create( &pThrs[i], NULL, thread_routine, (void *)(uintptr_t)i ))
        {
          errorexit( errno, "pthread_create", true);
        }
    }

  for (i = 0; i < procs; i++)
    pthread_join( pThrs[i], NULL );

  printf( "*****All done*******\n");

  free( pThrs );

  /* Print statistics ****************************************************************/
  printf("Statistics:\n");
  for (i=0; lifo_stat( pHead, i, &stat ) == ELIFO_OK; i++)
    {
      printf("%s: %lu\n", stat_label[i], stat );
    }
 
  printf( "\nDumping count|wait results...\n");
  for (i = 0; i < procs; i++)
    {
      printf("[%ld]%ld|%ld\n",i,global_count[i],global_wait[i]);
      total_count += global_count[i];
      total_wait  += global_wait[i];
    }

  printf( "Total:%ld|%ld\n",total_count,total_wait );

  free(global_count);
  free(global_wait);
  errorexit( lifo_cleanup_head( pHead, true ), "lifo_easy_cleanup", true);
  return 0;
}
