/*=====================================================================
 Copyright (C) 2012 Alain BENEDETTI alainb06@free.fr

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published byr
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>
=====================================================================*/

/*====================================================================================\
| Function:                                                                           |
|==========                                                                           |
| This is a basic example on how to use the LIFO library with static resources.       |
| This is NOT RECOMMENDED as you might find discrepencies depending on how lifo.c     |
| was compile. Nevertheless it is shown in case you really need it.                   |
\====================================================================================*/

#include <stdio.h>
#include "lifo.h"

static char head[SIZEOF_LIFO_HEAD];
struct user_node
  {
    int   number  ;
    long  whatever;
  };

static struct 
  {
    char   lifo_node[SIZEOF_LIFO_NODE];
    struct user_node user_node;
  } wrapped_user_node;


#define P_HEAD    ((struct lifo_head *)head)
#define LIFO_NODE ((struct lifo_node *)wrapped_user_node.lifo_node)

int
main()
{
  struct user_node *pUserNode= &(wrapped_user_node.user_node);
  struct user_node *pPoped;


/* STEP 1: init the Head =============================================================*/
/* IMPORTANT: head must be zeroed before using it that way. As it is static here, we  */
/*            already know it is zeroed, and we have nothing more to do.              */
  lifo_init_head( P_HEAD, LIFO_DCAS );


/* STEP 2: init the nodes you will use ===============================================*/
  lifo_init_node( LIFO_NODE );

  /* You would normally put some value in your node.
     This is your own user data here.                */
  pUserNode->number  = 1;
  pUserNode->whatever= 0;


/* STEP 3: push the node on the queue ================================================*/
  lifo_push( P_HEAD, pUserNode );

/* STEP 4: pop a node from the queue  ================================================*/
  lifo_pop ( P_HEAD, (void **)&pPoped );

  /* Now, here normally you got your own data back! */
  if (pPoped== NULL)
    {
      printf("No node was poped!\n");
    }
  else
    {
      printf("We poped the node number %d\n", pPoped->number);
    }


/* We don't need cleanup here, as head & nodes are static ============================*/

  printf("OK: all!\n");
  return 0;
}
