/*=====================================================================
 Copyright (C) 2012 Alain BENEDETTI alainb06@free.fr

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published byr
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>
=====================================================================*/

/*=====================================================================================\
| Function:                                                                            |
|==========                                                                            |
| This is a basic example on how to use the LIFO library for messages (MSG).           |
| We are assuming the library was compiled by default without checks, thus we test     |
| return values only on setups (in case allocations of memory went wrong)              |
|--------------------------------------------------------------------------------------|
| Date: 2013-01-07                                                                     |
|======                                                                                |
|                                                                                      |
|--------------------------------------------------------------------------------------|
| Author: Alain BENEDETTI aka Zakhar                                                   |
|========                                                                              |
|                                                                                      |
\=====================================================================================*/

#include <stdio.h>
#include "lifo.h"

#define NUM_NODES 3

int
main()
{
  int                 error;
  struct lifo_head   *pHead;
  struct user_node
   {
     int   number  ;
     long  whatever;
   }                 *pUserNode;
  struct user_node   *pPoped, *pNext;
  int                 i;


/* STEP 1: setup the Head ============================================================*/
  error= msg_setup_head( &pHead, LIFO_DEF_MODE );
  if (error != 0)
    {
      printf("Error %d on msg_setup_head\n", error);
      return error;
    }
  else
    {
      printf("OK: msg_setup_head\n");
    }


/* STEP 2: setup the nodes you will use ==============================================*/
  for (i=0; i<NUM_NODES; i++)
    {
      error= msg_setup_node( (void **)&pUserNode, sizeof(struct user_node) );
      if (error != 0)
        {
          printf("Error %d on msg_setup_node\n", error);
          return error;
        }
      else
        {
          printf("OK: msg_setup_node\n");
        }

      /* You would normally put some value in your node.
         This is your own user data here.                */
      pUserNode->number  = i + 1;
      pUserNode->whatever= 0;

/* STEP 3: push the node on the queue ================================================*/
      error= msg_push( pHead, pUserNode );
      if (error != ELIFO_OK)
        {
          printf("Error %d on msg_push\n", error);
          return error;
        }
      else
        {
          printf("OK: node %d pushed.\n", pUserNode->number);
        }
    }



/* STEP 4: pop a bunch of nodes from the queue =======================================*/
  msg_pop ( pHead, (void **)&pPoped );

/* STEP 5: we 'reverse' them to read them as FIFO ====================================*/
  pPoped= msg_reverse( pPoped );

  for (i=0; i<NUM_NODES; i++)
    {
      /* Now, here normally you got your own data back! */
      if (pPoped== NULL)
        {
          printf("No node was poped!\n");
          break;
        }
      else
        {
          printf("We poped the node number %d\n", pPoped->number);
/* STEP 6: we get the next node put the current one back on stack ====================*/
          pNext= msg_next( pPoped );
          msg_push( pHead, pPoped );
          pPoped= pNext;
        }
    }

/* STEP 7: pop a bunch of nodes from the queue =======================================*/
  msg_pop ( pHead, (void **)&pPoped );

/* Now they should be LIFO again, we don't reverse them and we have pushed them back 
   in the FIFO order */

  for (i=0; i<NUM_NODES; i++)
    {
      /* Now, here normally you got your own data back! */
      if (pPoped== NULL)
        {
          printf("No node was poped!\n");
          break;
        }
      else
        {
          printf("We poped the node number %d\n", pPoped->number);
/* STEP 8: we get the next node put the current one back on stack ====================*/
          pNext= msg_next( pPoped );
          msg_push( pHead, pPoped );
          pPoped= pNext;
        }
    }


/* STEP 9: cleanup of the head and the nodes altogether ==============================*/
  msg_cleanup_head(pHead, true);

  printf("OK: all!\n");
  return 0;
}
