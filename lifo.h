#ifndef __LIFO__
#define __LIFO__

#ifdef LEVEL1_DCACHE_LINESIZE
  #if LEVEL1_DCACHE_LINESIZE < 2
    #error "The cacheline size must be a power of 2 greater or equal than 2."
  #endif
#else
  #define LEVEL1_DCACHE_LINESIZE 64
#endif

#include <stdbool.h> /* This include is needed for: bool   */
#include <stddef.h>  /* This include is needed for: size_t */

#define ELIFO_OK        0
#define ELIFO_UNALIGNED 192
#define ELIFO_NULL      193
#define ELIFO_MODE_ERR  194
#define ELIFO_UNAVAIL   195
#define ELIFO_BUSY      196
#define ELIFO_UNINIT    197

/* Common stats to all modes    */
#define LIFO_STAT_PUSH_TOTAL          0
#define  MSG_STAT_PUSH_TOTAL          LIFO_STAT_PUSH_TOTAL
#define LIFO_STAT_POP_TOTAL           1
#define  MSG_STAT_POP_TOTAL           LIFO_STAT_POP_TOTAL
#define LIFO_STAT_POP_FAST_NULL       2
#define  MSG_STAT_POP_FAST_NULL       LIFO_STAT_POP_FAST_NULL


#define LIFO_STAT_LIMIT_NONE          LIFO_STAT_POP_FAST_NULL + 1
#define  MSG_STAT_LIMIT_NONE          LIFO_STAT_LIMIT_NONE


/* Common stats to all but NONE */
#define LIFO_STAT_PUSH_CONTENDED      3
#define  MSG_STAT_PUSH_CONTENDED      LIFO_STAT_PUSH_CONTENDED
#define LIFO_STAT_POP_CONTENDED       4
#define  MSG_STAT_POP_CONTENDED       LIFO_STAT_POP_CONTENDED
#define LIFO_STAT_POP_SLOW_NULL       5
#define  MSG_STAT_POP_SLOW_NULL       LIFO_STAT_POP_SLOW_NULL


#define LIFO_STAT_LIMIT_LOCK          LIFO_STAT_POP_SLOW_NULL + 1
#define  MSG_STAT_LIMIT_LOCK          LIFO_STAT_LIMIT_LOCK


/* Common stats to CAS and DCAS */
#define LIFO_STAT_PUSH_BACKOFFED      6
#define  MSG_STAT_PUSH_BACKOFFED      LIFO_STAT_PUSH_BACKOFFED
#define LIFO_STAT_PUSH_1st_BACKOFFS   7
#define  MSG_STAT_PUSH_1st_BACKOFFS   LIFO_STAT_PUSH_1st_BACKOFFS
#define LIFO_STAT_PUSH_SUP_BACKOFFS   8
#define  MSG_STAT_PUSH_SUP_BACKOFFS   LIFO_STAT_PUSH_SUP_BACKOFFS
#define LIFO_STAT_PUSH_TOTAL_ATOMICS  9
#define  MSG_STAT_PUSH_TOTAL_ATOMICS  LIFO_STAT_PUSH_TOTAL_ATOMICS
#define LIFO_STAT_PUSH_MAX_ATOMICS   10
#define  MSG_STAT_PUSH_MAX_ATOMICS    LIFO_STAT_PUSH_MAX_ATOMICS

#define LIFO_STAT_POP_BACKOFFED      11
#define  MSG_STAT_POP_BACKOFFED       LIFO_STAT_POP_BACKOFFED


#define  MSG_STAT_LIMIT_CAS           MSG_STAT_POP_BACKOFFED + 1
#define  MSG_STAT_LIMIT_DCAS          MSG_STAT_LIMIT_CAS
#define  MSG_STAT_LIMIT_ALL           MSG_STAT_LIMIT_CAS


#define LIFO_STAT_POP_1st_BACKOFFS   12
#define LIFO_STAT_POP_SUP_BACKOFFS   13
#define LIFO_STAT_POP_TOTAL_ATOMICS  14
#define LIFO_STAT_POP_MAX_ATOMICS    15


#define LIFO_STAT_LIMIT_DCAS          LIFO_STAT_POP_MAX_ATOMICS + 1


/* Stats specific to CAS */
#define LIFO_STAT_PUSH_HELPS         16
#define LIFO_STAT_POP_HELPS          17
#define LIFO_STAT_PUSH_OPTIMIZED     18
#define LIFO_STAT_POP_OPTIMIZED      19
#define LIFO_STAT_PUSH_DELAYED       20
#define LIFO_STAT_PUSH_EXCHANGED     21
#define LIFO_STAT_PUSH_CHAINED       22
#define LIFO_STAT_PUSH_LONGEST_CHAIN 23

#define LIFO_STAT_LIMIT_CAS           LIFO_STAT_PUSH_LONGEST_CHAIN + 1
#define LIFO_STAT_LIMIT_ALL           LIFO_STAT_LIMIT_CAS

typedef enum
  {
    LIFO_DEF_MODE= 0,
    LIFO_NONE,
    LIFO_LOCK,
    LIFO_CAS,
    LIFO_DCAS,
    LIFO_ERROR
  } lifo_mode;

#define msg_mode lifo_mode  /* Same mode for lifo and msg queues although DCAS being 
                               useless for msg, it will fallback silently to CAS      */

/*=====================================================================================\
| Data structure for 'head' of the LIFO queue and 'nodes'                              |
| They are not exposed on purpose. It is DISCOURAGED to use static or automatic        |
| storage for these structures as the CAS and DCAS algorithms need alignment, and for  |
| performance reason we align them on cachelines boundaries. When used as static or    |
| automatic storage, it will rely on the macro LEVEL1_DCACHE_LINESIZE to have the same |
| (or a multiple) value of the one used when compiling the library. Failing to have so |
| will result in errors throwned by the library functions or coredumps if pointer      |
| checks are disabled.                                                                 |
| This is why the RECOMMENDED usage is to use ONLY pointers to those structures and    |
| call the associated functions to allocate and free them. Doing so, the alignment     |
| and size will always be accurate.                                                    |
| Should you anyway need to allocate yourself or provide static structures, you can    |
| the sizes macro provided below.                                                      |
\=====================================================================================*/

#if __WORDSIZE == 64
# define SIZEOF_LIFO_HEAD 104
# define SIZEOF_LIFO_NODE  16
# define SIZEOF_MSG_NODE    8
#else
# define SIZEOF_LIFO_HEAD  88
# define SIZEOF_LIFO_NODE   8
# define SIZEOF_MSG_NODE    4
#endif

struct lifo_options
  {
    unsigned short int threshold;
    bool               stats    ;
    char               flags    ; /* internal use */
    lifo_mode          mode     ;
  };


struct lifo_head;
struct lifo_node;
struct msg_node;



#define msg_head lifo_head     /* At the moment, we use the same 'head' structure for */
                               /* LIFO and MSG. For convenience, and as it could      */
                               /* change, we recommend the use of this macro when you */
                               /* work with MSG.                                      */


int           lifo_setup_head   ( struct lifo_head   **ppHead  ,
                                         lifo_mode     mode      ) ;
int           lifo_init_head    ( struct lifo_head    *pHead  ,
                                         lifo_mode     mode      ) ;
int           lifo_cleanup_head ( struct lifo_head    *pHead  ,
                                  bool                 freeNodes ) ;

int           lifo_setup_stats  ( struct lifo_head    *pHead     ) ;
int           lifo_init_stats   ( struct lifo_head    *pHead  ,
                                  unsigned long       *pStats    ) ;
int           lifo_cleanup_stats( struct lifo_head    *pHead     ) ;

int           lifo_setup_node   ( void               **ppUserNode, 
                                  size_t               size      ) ;
int           lifo_init_node    ( struct lifo_node    *pUserNode ) ;
int           lifo_cleanup_node ( void                *pUserNode ) ;

int           lifo_get_threshold( struct lifo_head    *pHead  ,
                                  unsigned short int  *pThreshold) ;
int           lifo_set_threshold( struct lifo_head    *pHead  ,
                                  unsigned short int   threshold ) ;
int           lifo_get_stats    ( struct lifo_head    *pHead  ,
                                  bool                *pStats    ) ;
int           lifo_set_stats    ( struct lifo_head    *pHead  ,
                                  bool                 stats     ) ;
int           lifo_get_mode     ( struct lifo_head    *pHead  , 
                                         lifo_mode    *pMode     ) ;

int           lifo_stat         ( struct lifo_head    *pHead     ,
                                  unsigned int         statIndex ,
                                  unsigned long       *pStatCntr ) ;

int           lifo_push         ( struct lifo_head    *pHead  ,
                                  void                *pUserNode ) ;
int           lifo_pop          ( struct lifo_head    *pHead  ,
                                  void               **ppUserNode) ;


int            msg_setup_head   ( struct  msg_head   **ppHead  ,
                                          msg_mode     mode      ) ;
int            msg_init_head    ( struct  msg_head    *pHead  ,
                                          msg_mode     mode      ) ;
int            msg_cleanup_head ( struct  msg_head    *pHead  ,
                                  bool                freeNodes  ) ;

int            msg_setup_stats  ( struct  msg_head    *pHead     ) ;
int            msg_init_stats   ( struct  msg_head    *pHead  ,
                                  unsigned long       *pStats    ) ;
#define        msg_cleanup_stats(pHead)   lifo_cleanup_stats(pHead)

int            msg_setup_node   ( void               **ppUserNode, 
                                  size_t               size      ) ;
#define        msg_init_node(pUserNode)   ELIFO_OK
int            msg_cleanup_node ( void                *pUserNode ) ;

#define        msg_get_threshold(pHead,pThreshold)  \
                               lifo_get_threshold(pHead,pThreshold)
#define        msg_set_threshold(pHead,threshold)   \
                               lifo_set_threshold(pHead,threshold)
#define        msg_get_stats(    pHead,pStats)      \
                               lifo_get_stats(pHead,pStats)
#define        msg_set_stats(    pHead,stats)       \
                               lifo_set_stats(pHead,stats)
#define        msg_get_mode(     pHead,pMode)       \
                               lifo_get_mode(pHead,pMode)

#define        msg_stat(pHead,statIndex,pStatCntr)  \
                               lifo_stat(pHead,statIndex,pStatCntr)

int            msg_push         ( struct  msg_head    *pHead  ,
                                  void                *pUserNode );
int            msg_pop          ( struct  msg_head    *pHead  ,
                                  void               **ppUserNode);

void *         msg_next         ( void                *pUserNode );
void *         msg_reverse      ( void                *pUserNode );

extern const int                 lifo_features    ;
extern const int                 lifo_cache_align ;
extern const int                 lifo_sizeof_node ;
extern const struct lifo_options lifo_defaults    ;

/* These are macro to check compilation features of the library */
#define LIFO_IS_BACKOFF       (lifo_features &  1)
#define LIFO_IS_CHECK        ((lifo_features &  2) >> 1)
#define LIFO_IS_CAS_ENABLED  ((lifo_features &  4) >> 2)
#define LIFO_IS_TARGET_INTEL ((lifo_features &  8) >> 3)
#define LIFO_IS_64           ((lifo_features & 16) >> 4)
#endif /*__LIFO__*/
