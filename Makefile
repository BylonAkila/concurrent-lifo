# This is the directory where we put the examples
EXDIR   = examples

# Simple examples are TARGET1
TARGET1 = $(EXDIR)/lifo_1_basic         \
          $(EXDIR)/lifo_2_basic_static  \
          $(EXDIR)/lifo_3_msg           \
          $(EXDIR)/lifo_4_basic_stat

# TARGET2 are more sophisticated examples that need the -pthread LD flag
TARGET2 = $(EXDIR)/lifo_multi

CC      = gcc
CFLAGS  = -Wall -Wextra -std=gnu1x -D_GNU_SOURCE
LDFLAGS = -pthread

# Here are all our default LIFO configurations
LEVEL1_DCACHE_LINESIZE?=64
CHECK?=0
STATS?=0
CAS_ENABLED?=1
THRESHOLD?=-1
DEF_MODE?=4     # 1=LIFO_NONE, 2=LIFO_LOCK, 3=LIFO_CAS, 4=LIFO_DCAS
OPT_LEVEL?=-O3

all: $(TARGET1) $(TARGET2)

# For lifo.o, all the defaults config. options can be individually overwritten
#             passing the relevant parameter to make
lifo.o: lifo.c lifo.h
	$(CC) $(CFLAGS) $(OPT_LEVEL) -mcx16 \
              -DLEVEL1_DCACHE_LINESIZE=$(LEVEL1_DCACHE_LINESIZE) \
              -DCHECK=$(CHECK) \
              -DSTATS=$(STATS) \
              -DCAS_ENABLED=$(CAS_ENABLED) \
              -DTHRESHOLD=$(THRESHOLD) \
              -DDEF_MODE=$(DEF_MODE) $< -c -o $@

%.o: %.c lifo.h
	$(CC) $(CFLAGS) $(OPT_LEVEL) $< -c -o $@

# -pthread is needed anyway on target1 for stats which require pthread_mutex_trylock
.SECONDEXPANSION:
$(TARGET1): $$@.o lifo.o
	$(CC) $+ $(LDFLAGS) -o $@

$(TARGET2): $$@.o lifo.o
	$(CC) $+ $(LDFLAGS) -o $@

clean:
	rm -f $(TARGET1) $(TARGET2)
	rm -f lifo.o $(EXDIR)/*.o

call: clean all
